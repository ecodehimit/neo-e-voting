'use strict';

const KEY = 'user-theme-preference';
const ASSET_PATH = '/assets/theme-icon';

const buttons = document.querySelectorAll('.theme-button');
const theme = localStorage.getItem(KEY) ?? 'system';

if (theme) {
  switchTheme(theme);
}

if (theme === 'system') {
  window
    .matchMedia('(prefers-color-scheme: dark)')
    .addEventListener('change', () => {
      switchTheme(theme);
      updateButton(theme)
    });
}

if (buttons.length !== 0) {
  updateButton(theme);

  buttons.forEach((button) => {
    button.addEventListener('click', () => {
      const theme = button.id;

      switchTheme(theme);
      updateButton(theme);
    });
  });
}

function getPreferedUser() {
  return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
}

function switchTheme(theme) {
  localStorage.setItem(KEY, theme);
  document.querySelector('html').setAttribute(
    'data-bs-theme',
    theme === 'system' ? getPreferedUser() : theme
  );
}

function getIcon(theme) {
  if (theme === 'light') {
    return 'sun';
  } else if (theme === 'dark') {
    return 'moon';
  }

  return 'system';
}

function updateButton(theme) {
  const parentIcon = document.querySelector('#theme-parent-icon');
  const preferedTheme = theme === 'system' ? getPreferedUser() : theme;

  // change dropdown active item
  buttons.forEach((button) => {
    const icon = button.querySelector('img');
    const text = button.querySelector('span');
    const { id } = button;

    if (theme === id) {
      icon.setAttribute('src', `${ASSET_PATH}/${getIcon(theme)}-${preferedTheme}-active.svg`);
      text.classList.add('active-link');
    } else {
      icon.setAttribute('src', `${ASSET_PATH}/${getIcon(id)}-${preferedTheme}.svg`);
      text.classList.remove('active-link');
    }
  });

  // change icon dropdown button
  parentIcon.setAttribute('src', `${ASSET_PATH}/${getIcon(theme)}-${preferedTheme}-active.svg`);
}
