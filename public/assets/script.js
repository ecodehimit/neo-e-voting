const day = document.getElementById("day");
const hour = document.getElementById("hour");
const minute = document.getElementById("minute");
const second = document.getElementById("second");

const suksesi = "6 juli 2024 07:30:00";

// realtime countdown interval 1s
setInterval(() => {
    const suksesiDate = new Date(suksesi);
    const currentDate = new Date();

    const totalSeconds = (suksesiDate - currentDate) / 1000;

    const days = Math.floor(totalSeconds / 3600 / 24);
    const hours = Math.floor(totalSeconds / 3600) % 24;
    const minutes = Math.floor(totalSeconds / 60) % 60;
    const seconds = Math.floor(totalSeconds) % 60;

    day.innerHTML = (days < 10 ? "0" : "") + days;
    hour.innerHTML = (hours < 10 ? "0" : "") + hours;
    minute.innerHTML = (minutes < 10 ? "0" : "") + minutes;
    second.innerHTML = (seconds < 10 ? "0" : "") + seconds;
}, 1000);
const theme_button = document.querySelectorAll(".theme-button");
const kpu_logo = document.getElementById("kpu-logo");
const kpu_logo_dark = document.getElementById("kpu-dark-logo");
const suksesi_logo = document.getElementById("suksesi-logo");
const suksesi_logo_dark = document.getElementById("suksesi-dark-logo");
startTheme();
function startTheme() {
    const system_theme = localStorage.getItem("user-theme-preference");
    if (system_theme === "dark") {
        kpu_logo.style.display = "none";
        kpu_logo_dark.style.display = "block";
        suksesi_logo.style.display = "none";
        suksesi_logo_dark.style.display = "block";
    } else if (system_theme === "system") {
        const system_theme = window.matchMedia("(prefers-color-scheme: dark)");
        if (system_theme.matches) {
            kpu_logo.style.display = "none";
            kpu_logo_dark.style.display = "block";
            suksesi_logo.style.display = "none";
            suksesi_logo_dark.style.display = "block";
        } else {
            kpu_logo.style.display = "block";
            kpu_logo_dark.style.display = "none";
            suksesi_logo.style.display = "block";
            suksesi_logo_dark.style.display = "none";
        }
    }
    else {
        kpu_logo.style.display = "block";
        kpu_logo_dark.style.display = "none";
        suksesi_logo.style.display = "block";
        suksesi_logo_dark.style.display = "none";
    }

}
theme_button.forEach((button) => {
    button.addEventListener("click", () => {
        if (button.id === "light") {
            kpu_logo.style.display = "block";
            kpu_logo_dark.style.display = "none";
            suksesi_logo.style.display = "block";
            suksesi_logo_dark.style.display = "none";
        } else if (button.id === "dark") {
            kpu_logo.style.display = "none";
            kpu_logo_dark.style.display = "block";
            suksesi_logo.style.display = "none";
            suksesi_logo_dark.style.display = "block";
        } else {
            const system_theme = window.matchMedia("(prefers-color-scheme: dark)");
            if (system_theme.matches) {
                kpu_logo.style.display = "none";
                kpu_logo_dark.style.display = "block";
                suksesi_logo.style.display = "none";
                suksesi_logo_dark.style.display = "block";
            } else {
                kpu_logo.style.display = "block";
                kpu_logo_dark.style.display = "none";
                suksesi_logo.style.display = "block";
                suksesi_logo_dark.style.display = "none";
            }
        }
    });
});
