<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class Candidate extends Model
{

    use HasFactory;
    protected $guarded = [];    
    use LogsActivity;

    public function leader()
    {
        return $this->belongsTo(Student::class, 'candidate_lead');
    }
    public function vice()
    {
        return $this->belongsTo(Student::class, 'candidate_vice');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults();
    }
}
