<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusServer extends Model
{
    protected $table = 'status_servers';
    protected $fillable = [
        'status'
    ];
    use HasFactory;
}
