<?php

namespace App\Jobs;

use App\Models\Calculation;
use App\Services\Student\StudentService;
use Faker\Factory as Faker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class votingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $student_id;
    protected $candidate_choice;

    protected $studentService;

    public function __construct($student_id,$candidate_choice, $studentService)
    {
        $this->student_id = $student_id;
        $this->candidate_choice = $candidate_choice;
        $this->studentService = $studentService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sleep(3);

//        info();

        try {
            $this->studentService->votingAction($this->student_id,$this->candidate_choice);

            info('success add id : '.$this->student_id);
        }catch (\Exception $exception){
            info($exception);
        }


    }
}
