<?php

namespace App\Services\Home;

use LaravelEasyRepository\Service;
use App\Repositories\Home\HomeRepository;

class HomeServiceImplement extends Service implements HomeService{

     /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
     protected $mainRepository;

    public function __construct(HomeRepository $mainRepository)
    {
      $this->mainRepository = $mainRepository;
    }

    // Define your custom methods :)
}
