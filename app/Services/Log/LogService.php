<?php

namespace App\Services\Log;

use LaravelEasyRepository\BaseService;

interface LogService extends BaseService{

    // Write something awesome :)
    public function getDataTable($option);
}
