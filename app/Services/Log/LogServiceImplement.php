<?php

namespace App\Services\Log;

use LaravelEasyRepository\Service;
use App\Repositories\Log\LogRepository;
use App\Repositories\User\UserRepository;
use Yajra\DataTables\DataTables;

class LogServiceImplement extends Service implements LogService{

     /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
     protected $mainRepository, $userRepository;

    public function __construct(LogRepository $mainRepository, UserRepository $userRepository)
    {
      $this->mainRepository = $mainRepository;
      $this->userRepository = $userRepository;
    }

    // Define your custom methods :)
    public function getDataTable($option)
    {
        $data = $this->mainRepository->getLatestData();
        $dataTable = DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('causer_id', function($row)
            {
                $student = $this->userRepository->getUserById($row->causer_id);
                if ($student != null) {
                    return $student->name;
                } else {
                    return '';
                }
            })      
            ->addColumn('created_at', function($row)
            {
               $date = date("d F Y, H:i:s", strtotime($row->created_at));
               return $date;
            })      
            ->make(true);
        return $dataTable;
    }
}
