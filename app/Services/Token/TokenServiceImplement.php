<?php

namespace App\Services\Token;

use App\Models\Mahasiswa;
use App\Models\Student;
use App\Models\Token;
use Carbon\Carbon;
use LaravelEasyRepository\Service;
use App\Repositories\Token\TokenRepository;
use App\Repositories\Student\StudentRepository;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class TokenServiceImplement extends Service implements TokenService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository, $studentRepository;

    public function __construct(TokenRepository $mainRepository, StudentRepository $studentRepository)
    {
        $this->mainRepository = $mainRepository;
        $this->studentRepository = $studentRepository;
    }

    public function getDataTable($option)
    {
        $data = $this->mainRepository->getDataActive($option);
        //        dd($data);
        //        dd($data);
        $dataTambles = DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('duration', function ($data) {
                $time_in_minutes = 5;
                $now  = Carbon::now();
                $expiredAt = new \DateTime($data->expired_at);
                $currentTime = new \DateTime();
                $isExpired = $expiredAt <= $currentTime || $data->use_time != null;
                return '<a class="timer" data-create="' . $data->updated_at . '" data-id="' . $data->id . '" data-status="' . $isExpired . '"  data-dateNow="' . $now . '"></a>';                
            })
            ->addColumn('updated_at', function ($data) {
                $data = Carbon::parse($data->updated_at);
                return $data;
            })
            ->addColumn('action', function ($data) {
                //                $button = 'dqw';
                $button = '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Delete" data-nrp="' . $data->student->student_nrp . '" data-name="' . $data->student->student_name . '" class="btn btn-info regenerate">regenerate</a></div>';
                return '<div class="row p-1">' . $button . '</div>';
            })
            ->rawColumns(['duration', 'action'])
            ->make(true);
        return $dataTambles;
    }

    public function updateToken($id)
    {
        $data = $this->mainRepository->updateToken($id);
        return $data;
    }
    public function store($request)
    {
        $random = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = substr(str_shuffle(str_repeat($random, 5)), 0, 5);

        $student_id = $request->student_id;
        $count = Token::where('student_id', $student_id)->count();
        $cek_student = Student::where([['id', '=', $student_id], ['status', '=', 'yes']])->count();
        if ($count > 0 || $cek_student > 0) {
            if ($cek_student > 0) {
                return [
                    'status' => false,
                    'message' => 'Kamu Sudah menggunkan hak pilih'
                ];
            }

            return [
                'status' => false,
                'message' => 'Kamu Sudah punya token'
            ];
        }

        activity()
            ->causedBy(Auth::id())
            ->event('generate token ' . $this->studentRepository->find($student_id)->name)
            ->log('generate token ' . $this->studentRepository->find($student_id)->name);
        //        $result = Token::query()->create($req);
        $data = $this->mainRepository->store($student_id, $token);
        return [
            'status' => true,
            'message' => 'Berhasil generate token'
        ];
    }

    public function regenerate($id)
    {
        $random = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $new_token = substr(str_shuffle(str_repeat($random, 5)), 0, 5);

        $token = Token::where('id', $id)->with(['student'])->first();

        $expired_at = new \DateTime("+5 minutes");

        //        dd($token);
        //
        if ('yes' == $token->student->status) {
            return [
                'status' => false,
                'message' => 'Kamu sudah menggunakan hak pilihmu'
            ];
        }

        if ($token->count < 1) {
            activity()
                ->causedBy(Auth::id())
                ->event('regenerate token ' . $this->studentRepository->find($token->student_id)->name)
                ->log('regenerate token ' . $this->studentRepository->find($token->student_id)->name);
            $update = Token::where('id', $id)->update(['count' => $token->count + 1, 'token' => $new_token, 'expired_at' => $expired_at]);

            return [
                'status' => true,
                'message' => 'Berhasil regenerate token'
            ];
        }

        return [
            'status' => false,
            'message' => 'Kamu sudah melebihi batas regenerate token'
        ];

        // $result = Token::query()->create($req);
        // $data = $this->mainRepository->store($student_id, $token);
    }

    public function getByStudent($student_id)
    {
        $data = $this->mainRepository->getByStudent($student_id);

        return $data;
    }
}
