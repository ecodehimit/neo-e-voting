<?php

namespace App\Services\Token;

use LaravelEasyRepository\BaseService;

interface TokenService extends BaseService
{

    // Write something awesome :)
    public function getDataTable($option);

    public function updateToken($id);
    public function store($request);
    public function regenerate($request);
    public function getByStudent($student_id);
}
