<?php

namespace App\Services\LoginStudent;

use LaravelEasyRepository\BaseService;

interface LoginStudentService extends BaseService{

    // Write something awesome :)

    public function login($request);
}
