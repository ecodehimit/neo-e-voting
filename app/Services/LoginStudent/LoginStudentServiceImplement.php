<?php

namespace App\Services\LoginStudent;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use LaravelEasyRepository\Service;
use App\Repositories\LoginStudent\LoginStudentRepository;
use Spatie\Activitylog\Models\Activity;
use App\Models\Student;
use App\Models\User;

class LoginStudentServiceImplement extends Service implements LoginStudentService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository;

    public function __construct(LoginStudentRepository $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }
    public function validateLogin($request)
    {

        //        dd(env('DURATION_TOKEN'));
        $message = '';
        $data = null;

        $message = [
            'status' => null,
            'message' => $message,
            'data' => $data
        ];

        $student = $this->mainRepository->getStudent('student_nrp', $request->nrp);

        if ($student) {
            $token = $this->mainRepository->getTokenByStudentId($student->id, $request->token);

            if ($token) {
                if (new \DateTime($token->expired_at) <= new \DateTime || $token->use_time != null) {

                    //                    dd('dqw');
                    return $message = [
                        'status' => 'warning',
                        'message' => 'Token Expired woyyy >:( ',
                        'data' => $data
                    ];
                }


                $data['student'] = $student;
                $data['token'] = $token;


                $message = [
                    'status' => 'success',
                    'message' => 'get student :) ',
                    'data' => $data
                ];
            } else {
                return $message = [
                    'status' => 'danger',
                    'message' => 'Token not found >:( ',
                    'data' => $data
                ];
            }
        } else {
            $message = [
                'status' => 'danger',
                'message' => 'NRP not found >:(',
                'data' => $data
            ];
        }

        return $message;
    }

    public function login($request)
    {

        $validateLogin = $this->validateLogin($request);

        //        dd($validateLogin);

        $auth = $validateLogin;


        if ($validateLogin['status'] == 'success') {
            //            dd($validateLogin['data']['token']['id']);
            //            dd($validateLogin['data']['token']);
            $s = $validateLogin['data']['token'];

            $login = Auth::guard('voter')->loginUsingId($validateLogin['data']['token']['id']);
            if ($login) {
                //                return redirect()->route('vote');
                //                $request->session()->regenerate();

                // log activity
                $student = $this->mainRepository->getStudent('student_nrp', $request->nrp);
                activity()
                    ->causedBy(Auth::id())
                    ->performedOn($student)
                    ->event('login')
                    ->log('login');
                // dd($validateLogin['data']['token']['id']);
                // $data = $this->mainRepository->updateToken($validateLogin['data']['token']['id']);
                // dd($data);
            }
            //
            //            $request->session()->regenerate();

        }

        return $auth;
    }


    // Define your custom methods :)
}
