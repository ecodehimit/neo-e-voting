<?php

namespace App\Services\User;

use LaravelEasyRepository\Service;
use App\Repositories\User\UserRepository;
use Yajra\DataTables\DataTables;

class UserServiceImplement extends Service implements UserService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository;

    public function __construct(UserRepository $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }

    public function getDataTable($option)
    {
        $data = $this->mainRepository->all();
        $dataTable = DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                $button = '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary editProduct"><i class="fa fa-edit"></i></a></div>';
                $button .= '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Delete" class="btn btn-danger  deleteProduct"><i class="fa fa-trash"></i></a></div>';
                return '<div class="row p-1">' . $button . '</div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        return $dataTable;
    }

    public function getSaksiList()
    {
        return $this->mainRepository->getSaksiList();
    }

    public function getUserById($id)
    {
        return $this->mainRepository->getUserById($id);
    }
    // Define your custom methods :)
}
