<?php

namespace App\Services\User;

use LaravelEasyRepository\BaseService;

interface UserService extends BaseService
{

    // Write something awesome :)
    public function getDataTable($option);
    public function getSaksiList();
    public function getUserById($id);
}
