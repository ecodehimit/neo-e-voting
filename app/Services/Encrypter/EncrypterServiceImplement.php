<?php

namespace App\Services\Encrypter;

use LaravelEasyRepository\Service;
use App\Repositories\Encrypter\EncrypterRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Js;

class EncrypterServiceImplement extends Service implements EncrypterService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $url, $user, $pass;

    public function __construct()
    {
        $this->url = env('API_SERVER');
        $this->user = env('API_USER');
        $this->pass = env('API_PASSWORD');
    }

    public function encryptVote($vote)
    {
        // dd($this->url, $this->user, $this->pass);
        $client = new Client();
        // set header
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'username' => $this->user,
            'password' => $this->pass,
        ];

        $body = [
            'data' => $vote
        ];

        $response = $client->request('POST', $this->url . '/cryptos/encrypt', [
            'json' => $body
        ]);

        $result = json_decode($response->getBody()->getContents(), true);
        return $result['data'];
    }

    public function decryptVote($vote)
    {

        // set header
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'username' => $this->user,
            'password' => $this->pass,
        ];

        $body = [
            'data' => $vote
        ];

        $client = new Client();
        $promise = $client->requestAsync('POST', $this->url . '/cryptos/decrypt', [
            'json' => $body
        ])->then(function ($response) {
            $result = json_decode($response->getBody()->getContents(), true);
            return $result['data'];
        });

        $promise->wait();
        return $promise->wait();
    }

    // Define your custom methods :)
}
