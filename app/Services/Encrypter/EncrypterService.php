<?php

namespace App\Services\Encrypter;

use LaravelEasyRepository\BaseService;

interface EncrypterService extends BaseService
{
    public function encryptVote($vote);
    public function decryptVote($vote);
}
