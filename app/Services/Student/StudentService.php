<?php

namespace App\Services\Student;

use LaravelEasyRepository\BaseService;

interface StudentService extends BaseService
{

    // Write something awesome :)
    public function getDataTable($option);
    public function store($request);
    public function count($option);
    public function votingAction($student_id, $candidate_choice);
    public function totalPerangkatan();
}
