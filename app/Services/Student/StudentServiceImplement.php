<?php

namespace App\Services\Student;

use App\Models\Calculation;
use App\Models\Student;
use LaravelEasyRepository\Service;
use App\Repositories\Student\StudentRepository;
use Yajra\DataTables\DataTables;

class StudentServiceImplement extends Service implements StudentService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository;

    public function __construct(StudentRepository $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }
    public function getDataTable($option)
    {
        //        dd('dqwdqw');
        $data = $this->mainRepository->getDataActive($option);

        //        dd($data);
        $dataTambles = DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                //                $button = 'dqw';
                $button = '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary editProduct"><i class="fa fa-edit"></i></a></div>';
                $button .= '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Delete" class="btn btn-danger  deleteProduct"><i class="fa fa-trash"></i></a></div>';
                return '<div class="row p-1">' . $button . '</div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        return $dataTambles;
    }

    public function store($request)
    {
        $data = $this->mainRepository->store($request);
        return $data;
    }

    public function count($option)
    {
        $data = $this->mainRepository->count($option);
        //        dd($data);
        return $data;
    }

    public function votingAction($student_id, $candidate_choice)
    {
        $data = new Calculation();
        $data->student_id = $student_id;
        $data->candidate_choice = $candidate_choice;
        $data->save();

        $student = $this->mainRepository->find($student_id);
        $student->status = 'yes';
        $student->save();

        return $data;
    }

    public function totalPerangkatan()
    {
        $data = $this->mainRepository->totalPerangkatan();
        return $data;
    }
    // Define your custom methods :)
}
