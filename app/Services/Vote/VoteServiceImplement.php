<?php

namespace App\Services\Vote;

use LaravelEasyRepository\Service;
use App\Repositories\Vote\VoteRepository;

class VoteServiceImplement extends Service implements VoteService{

     /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
     protected $mainRepository;

    public function __construct(VoteRepository $mainRepository)
    {
      $this->mainRepository = $mainRepository;
    }

    // Define your custom methods :)
}
