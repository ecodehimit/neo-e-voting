<?php

namespace App\Services\Candidate;

use LaravelEasyRepository\BaseService;

interface CandidateService extends BaseService
{

    // Write something awesome :)
    public function getDataTable($option);
    public function store($request);
    public function count($option);
}
