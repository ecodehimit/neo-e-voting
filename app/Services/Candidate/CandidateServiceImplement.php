<?php

namespace App\Services\Candidate;

use LaravelEasyRepository\Service;
use App\Repositories\Candidate\CandidateRepository;
use Yajra\DataTables\DataTables;

class CandidateServiceImplement extends Service implements CandidateService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository;

    public function __construct(CandidateRepository $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }

    public function getDataTable($option)
    {
//        dd('dqwdqw');
        $data = $this->mainRepository->getDataActive($option);

//        dd($data);
        $dataTambles = DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('photo',function($data){
                $url= asset('storage/storage/assets_softUI/img/' . $data->photo);
                return '<img src="' . $url . '" class="rounded img-thumbnail" align="center" />';
            })
            ->editColumn('candidate_lead', function ($data) {
                if ($data->leader == null) return '';
                return $data->leader->student_name;
            })
            ->editColumn('candidate_vice', function ($data) {
                if ($data->vice == null) return '';
                return $data->vice->student_name;
            })
            ->addColumn('action', function($data){
//                $button = 'dqw';
                $button = '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Edit" class="edit btn btn-primary editProduct"><i class="fa fa-edit"></i></a></div>';
                $button .= '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$data->id.'" data-original-title="Delete" class="btn btn-danger  deleteProduct"><i class="fa fa-trash"></i></a></div>';
                return '<div class="row p-1">' . $button . '</div>';
            })
            ->rawColumns(['photo','action','candidate_lead','candidate_vice'])
            ->make(true);
        return $dataTambles;
    }

    public function store($request)
    {
        $data = $this->mainRepository->store($request);
        return $data;
    }

    public function count($option)
    {
        $data = $this->mainRepository->count($option);
//        dd($data);
        return $data;

    }
}
