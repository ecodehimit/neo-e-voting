<?php

namespace App\Services\Group;

use LaravelEasyRepository\Service;
use App\Repositories\Group\GroupRepository;
use Yajra\DataTables\DataTables;

class GroupServiceImplement extends Service implements GroupService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository;

    public function __construct(GroupRepository $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }

    // Define your custom methods :)
    public function getDataTable($option)
    {
        //        dd('dqwdqw');
        $data = $this->mainRepository->getDataActive($option);

        //        dd($data);
        $dataTambles = DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                $button = '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary editProduct"><i class="fa fa-edit"></i></a></div>';
                $button .= '<div class="col-auto p-1"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Delete" class="btn btn-danger  deleteProduct"><i class="fa fa-trash"></i></a></div>';
                return '<div class="row p-1">' . $button . '</div>';
            })
            ->rawColumns(['action'])
            ->make(true);
        return $dataTambles;
    }

    public function store($request)
    {
        $data = $this->mainRepository->store($request);
        return $data;
    }

    public function count($option)
    {
        $data = $this->mainRepository->count($option);
        return $data;
    }

    public function searchIdByGroupName($groupName)
    {
        return $this->mainRepository->searchIdByGroupName($groupName);
    }
}
