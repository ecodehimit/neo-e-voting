<?php

namespace App\Services\Group;

use LaravelEasyRepository\BaseService;

interface GroupService extends BaseService
{

    // Write something awesome :)
    public function getDataTable($option);
    public function store($request);
    public function count($option);
    public function searchIdByGroupName($groupName);
}
