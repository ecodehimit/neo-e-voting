<?php

namespace App\Services\Calculation;

use LaravelEasyRepository\Service;
use App\Repositories\Calculation\CalculationRepository;

class CalculationServiceImplement extends Service implements CalculationService
{

    /**
     * don't change $this->mainRepository variable name
     * because used in extends service class
     */
    protected $mainRepository;

    public function __construct(CalculationRepository $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }

    public function getDataVote()
    {
        $data = $this->mainRepository->getDataVote();
        return $data;
    }
}
