<?php

namespace App\Repositories\Student;

use App\Models\Student;
use LaravelEasyRepository\Implementations\Eloquent;
use Illuminate\Support\Facades\DB;

class StudentRepositoryImplement extends Eloquent implements StudentRepository
{

    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */
    protected $model;

    public function __construct(Student $model)
    {
        $this->model = $model;
    }

    public function getDataActive($option)
    {
        $data = $this->model->with(['group'])->where('deleted_at', $option)->orderBy('updated_at', 'desc');
        return $data;
    }

    public function store($request)
    {
        //        dd($request);
        $data = $this->model::updateOrCreate(
            [
                'id' => $request->data_id
            ],
            [
                'student_name' => $request->student_name,
                'student_nrp' => $request->student_nrp,
                'status' => $request->status,
                'group_id' => $request->group_id,
            ]
        );
        return $data;
    }

    public function count($option)
    {
        if ($option) {
            $data = $this->model->whereStatus($option)->where('deleted_at', null)->count();
        } else {
            $data = $this->model->where('deleted_at', null)->count();
        }
        return $data;
    }

    public function search($search)
    {
        $data = $this->model->where('student_nrp', 'LIKE', '%' . $search . '%')->get();
        return $data;
    }

    public function totalPerangkatan()
    {
        //    groupby and count by status yes or no if not exist create new array
        // dd($angkatan);
        $data = $this->model->with(['group'])->whereHas('group')->groupBy('status')->groupBy('group_id')->select('status', 'group_id', \DB::raw('count(*) as total'))->get();
        return $data;
    }


    public function find($id)
    {
        $data = $this->model->with(['group'])->where('id', $id)->first();
        return $data;
    }

    public function login($request)
    {
        // TODO: Implement login() method.
    }
    // Write something awesome :)
}
