<?php

namespace App\Repositories\Student;

use LaravelEasyRepository\Repository;

interface StudentRepository extends Repository
{

    // Write something awesome :)
    public function getDataActive($option);
    public function store($request);
    public function count($option);
    public function search($search);

    public function login($request);
    public function totalPerangkatan();
    public function find($id);
}
