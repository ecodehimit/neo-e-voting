<?php

namespace App\Repositories\LoginStudent;

use LaravelEasyRepository\Repository;

interface LoginStudentRepository extends Repository
{

    // Write something awesome :)
    public function getStudent($key, $value);
    public function getToken($key, $value);
    public function getTokenByStudentId($studentId, $token);
    public function updateToken($id);
}
