<?php

namespace App\Repositories\LoginStudent;

use App\Models\Token;
use App\Models\Student;
use LaravelEasyRepository\Implementations\Eloquent;

class LoginStudentRepositoryImplement extends Eloquent implements LoginStudentRepository
{

    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */
    protected $model;

    public function __construct(Token $model)
    {
        $this->model = $model;
    }

    public function getStudent($key, $value)
    {
        return Student::where($key, $value)->first();
    }

    public function getToken($key, $value)
    {
        return Token::where($key, $value)->first();
    }

    public function getTokenByStudentId($studentId, $token)
    {
        $data = $this->getToken('student_id', $studentId);
        if ($data) {
            return $data->where('token', $token)->first();
        } return false;
    }

    public function updateToken($id)
    {
        $token = $this->model->find($id);
        $token->use_time = date('Y-m-d H:i:s');
        $token->save();
    }
}
