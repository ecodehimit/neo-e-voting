<?php

namespace App\Repositories\Token;

use LaravelEasyRepository\Repository;

interface TokenRepository extends Repository
{

    // Write something awesome :)
    public function getDataActive($option);
    public function updateToken($id);
    public function store($student_id, $token);
    public function getByStudent($student_id);
}
