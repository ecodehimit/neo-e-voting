<?php

namespace App\Repositories\Token;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Token;

class TokenRepositoryImplement extends Eloquent implements TokenRepository
{

    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */
    protected $model;

    public function __construct(Token $model)
    {
        $this->model = $model;
    }
    public function getDataActive($option)
    {
        $data = $this->model->with(['student'])->orderBy('updated_at', 'DESC');
        return $data;
    }

    public function updateToken($id)
    {
        $data = $this->model->find($id);

        $data->save();

        return $data;
    }

    public function store($student_id, $token)
    {
        $data = new $this->model();
        $data->student_id = $student_id;
        $data->token = $token;
        $data->expired_at = new \DateTime("+5 minutes");
        $data->save();

        return $data;
    }

    public function getByStudent($student_id)
    {
        $data = $this->model->where('student_id', $student_id)->first();
        return $data;
    }
    // Write something awesome :)
}
