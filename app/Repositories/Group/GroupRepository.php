<?php

namespace App\Repositories\Group;

use LaravelEasyRepository\Repository;

interface GroupRepository extends Repository
{

    // Write something awesome :)
    public function getDataActive($option);
    public function store($request);
    public function count($option);
    public function search($search);
    public function searchIdByGroupName($groupName);
}
