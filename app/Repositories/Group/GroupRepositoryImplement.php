<?php

namespace App\Repositories\Group;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Group;

class GroupRepositoryImplement extends Eloquent implements GroupRepository
{

    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */
    protected $model;

    public function __construct(Group $model)
    {
        $this->model = $model;
    }

    // Write something awesome :)
    public function getDataActive($option)
    {
        $data = $this->model->where('deleted_at', $option)->orderBy('updated_at', 'desc');
        return $data;
    }

    public function store($request)
    {
        $data = $this->model::updateOrCreate(
            [
                'id' => $request->data_id
            ],
            [
                'group_name' => $request->group_name,
            ]
        );
        return $data;
    }

    public function count($option)
    {
        if ($option) {
            $data = $this->model->whereStatus($option)->where('deleted_at', null)->count();
        } else {
            $data = $this->model->where('deleted_at', null)->count();
        }
        return $data;
    }

    public function search($search)
    {
        $data = $this->model->where('group_name', 'LIKE', '%' . $search . '%')->get();
        return $data;
    }
    public function searchIdByGroupName($groupName)
    {
        $data = $this->model->where('group_name', $groupName)->first();
        return $data->id;
    }
}
