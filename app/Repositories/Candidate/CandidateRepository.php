<?php

namespace App\Repositories\Candidate;

use LaravelEasyRepository\Repository;

interface CandidateRepository extends Repository
{

    // Write something awesome :)
    public function getDataActive($option);
    public function count($option);
    public function store($request);
    public function search($search);
}
