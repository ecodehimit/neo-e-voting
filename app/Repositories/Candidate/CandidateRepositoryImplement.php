<?php

namespace App\Repositories\Candidate;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Candidate;

class CandidateRepositoryImplement extends Eloquent implements CandidateRepository
{

    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */
    protected $model;

    public function __construct(Candidate $model)
    {
        $this->model = $model;
    }

    public function getDataActive($option)
    {
        $data = $this->model->with(['leader','vice'])->where('deleted_at',$option)->orderBy('updated_at','desc');
        return $data;

    }

    public function count($option)
    {
        if ($option){
            $data = $this->model->whereStatus($option)->where('deleted_at',null)->count();
        }else{
            $data = $this->model->where('deleted_at',null)->count();
        }
        return $data;
    }

    public function storeImage($request){
        $photo = null;
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $photo = time() . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public/storage/assets_softUI/img'),$photo);
        }
        return  $photo;
    }

    public function store($request)
    {
        // dd($request);
        if ($request->data_id) {
            $data = $this->model->find($request->data_id);
            if ($request->hasFile('photo')) {
                $photo = $this->storeImage($request);
            } else {
                $photo = $data->photo;
            }
        }else{
            if ($request->hasFile('photo')) {
                $photo = $this->storeImage($request);
            }else{
                $photo = null;
            }
        }

        $data = $this->model::updateOrCreate(
            [
                'id' => $request->data_id
            ],[
                'candidate_lead' => $request->candidate_lead,
                'candidate_vice' => $request->candidate_vice,
                'number' => $request->number,
                'photo' => $photo,
                'visi' => $request->visi,
                'mission' => $request->mission
            ]
        );
        return $data;
    }

    public function search($search)
    {
        $data = $this->model->where('candidate_lead', 'LIKE', '%'. $search. '%')->get();
        return $data;
    }
}
