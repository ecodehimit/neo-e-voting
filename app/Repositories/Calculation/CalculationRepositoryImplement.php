<?php

namespace App\Repositories\Calculation;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\Calculation;
use App\Models\Candidate;
use App\Services\Encrypter\EncrypterService;
use Illuminate\Support\Facades\Crypt;

class CalculationRepositoryImplement extends Eloquent implements CalculationRepository
{

    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */
    protected $model, $decrypted;

    public function __construct(Calculation $model, EncrypterService $decrypted)
    {
        $this->model = $model;
        $this->decrypted = $decrypted;
    }
    public function getDataVote()
    {
        $data = $this->model->get();
        foreach ($data as $key => $calculation) {
            $calculation->candidate_choice = $this->decrypted->decryptVote($calculation->candidate_choice);
            $calculation->candidate_choice = Crypt::decryptString($calculation->candidate_choice);
            $candidate = Candidate::with(['leader', 'vice'])->find($calculation->candidate_choice);
            $calculation->candidate_name = $candidate->leader->student_name . ' & ' . $candidate->vice->student_name;
        }
        $grouped_data = $data->groupBy('candidate_choice')->map(function ($group, $key) {
            return [
                'candidate_choice' => $key,
                'total' => count($group),
                'candidate_name' => $group[0]->candidate_name,
                'data' => $group,
            ];
        });
        return $grouped_data;
    }
}
