<?php

namespace App\Repositories\Calculation;

use LaravelEasyRepository\Repository;

interface CalculationRepository extends Repository
{
    public function getDataVote();
}
