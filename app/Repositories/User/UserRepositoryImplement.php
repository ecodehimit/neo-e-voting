<?php

namespace App\Repositories\User;

use LaravelEasyRepository\Implementations\Eloquent;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepositoryImplement extends Eloquent implements UserRepository
{

    /**
     * Model class to be used in this repository for the common methods inside Eloquent
     * Don't remove or change $this->model variable name
     * @property Model|mixed $model;
     */
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create($request)
    {

        // check if exist id
        $user = null;
        if ($request['data_id'] != null) {
            $user = $this->model->find($request['data_id']);
        }

        $data = $this->model->updateOrCreate(
            [
                'id' => $request['data_id']
            ],
            [
                'name' => $request['name'],
                'email' => $request['email'],
                'role' => $request['role'],
                'nrp' => $request['nrp'],
                'description' => $request['description'],
                'password' => ($user != null) ? $user->password : Hash::make($request['password']),
            ]
        );
        return $data;
    }

    public function all()
    {
        $data = $this->model->all();
        return $data;
    }

    public function show($id)
    {
        $data = $this->model->find($id);
        return $data;
    }

    public function delete($id)
    {
        $data = $this->model->find($id);
        $data->delete();
        return $data;
    }

    public function getSaksiList()
    {
        $data = $this->model->where('role', 'saksi')->get();
        return $data;
    }

    public function getUserById($id)
    {
        $data = $this->model->where('id', $id)->first();
        return $data;
    }

    // Write something awesome :)
}
