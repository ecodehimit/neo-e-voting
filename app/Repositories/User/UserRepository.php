<?php

namespace App\Repositories\User;

use LaravelEasyRepository\Repository;

interface UserRepository extends Repository
{

    // Write something awesome :)

    public function all();
    // public function store($request);
    public function create($data);
    public function show($id);
    public function delete($id);
    public function getSaksiList();
    public function getUserById($id);
}
