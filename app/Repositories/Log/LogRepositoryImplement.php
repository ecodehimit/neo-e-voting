<?php

namespace App\Repositories\Log;

use LaravelEasyRepository\Implementations\Eloquent;
use Spatie\Activitylog\Models\Activity;

class LogRepositoryImplement extends Eloquent implements LogRepository{

    /**
    * Model class to be used in this repository for the common methods inside Eloquent
    * Don't remove or change $this->model variable name
    * @property Model|mixed $model;
    */
    protected $model;

    public function __construct(Activity $model)
    {
        $this->model = $model;
    }

    // Write something awesome :)
    public function getLatestData() {
        return Activity::latest()->get();
    }
}
