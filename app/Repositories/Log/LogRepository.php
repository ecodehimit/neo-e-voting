<?php

namespace App\Repositories\Log;

use LaravelEasyRepository\Repository;

interface LogRepository extends Repository{

    // Write something awesome :)
    public function getLatestData();
}
