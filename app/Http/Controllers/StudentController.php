<?php

namespace App\Http\Controllers;

use App\Events\QuickCountUpdated;
use App\Http\Requests\LoginStudentRequest;
use App\Jobs\votingJob;
use App\Models\Candidate;
use App\Models\Token;
use App\Repositories\LoginStudent\LoginStudentRepository;
use App\Services\LoginStudent\LoginStudentService;
use App\Services\Student\StudentService;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Services\Token\TokenService;
use Illuminate\Support\Carbon;
use App\Services\Encrypter\EncrypterService;

class StudentController extends Controller
{
    public $loginStudentService, $studentService, $tokenService, $encrypted, $loginRepository;

    public function __construct(LoginStudentService $loginStudentService, StudentService $studentService, TokenService $tokenService, EncrypterService $encrypted, LoginStudentRepository $loginRepository)
    {
        $this->loginStudentService = $loginStudentService;
        $this->studentService = $studentService;
        $this->encrypted = $encrypted;
        $this->tokenService = $tokenService;
        $this->loginRepository = $loginRepository;
    }

    public function index()
    {

        return view('login');
    }

    public function actionLogin(LoginStudentRequest $request)
    {
        $data = $this->loginStudentService->login($request);

        if ($data['status'] == 'success') {
            $data = $this->loginRepository->updateToken($data['data']['token']['id']);
            return redirect()->route('vote');
        } else {
            return redirect()->back()->with($data['status'], $data['message']);
        }
    }

    public function logoutLogActivity()
    {
        activity()
            ->causedBy(Auth::id())
            ->event('logout')
            ->log('logout');
        Auth::guard('voter')->logout();
    }


    public function vote()
    {

        $candidates = Candidate::with(['leader', 'vice'])->WhereNull('deleted_at')
            ->where('number', '!=', '0')
            ->orderBy('number', 'ASC')->get();
        $use_time = $this->tokenService->getByStudent(auth()->guard('voter')->user()->student->id)->use_time;
        // dd($use_time);
        $expired_time = Carbon::parse($use_time)->addMinutes(10);
        //        dd($candidates);
        view()->share([
            'expired_time' => $expired_time,
            'candidates' => $candidates
        ]);
        return view('vote');
    }

    public function vote_succes()
    {
        $this->logoutLogActivity();
        session()->flash('thank', '"Thank you for participating and voting in this election, your vote
                                is
                                very
                                valuable"');
        return view('vote_success');
    }

    public function logout()
    {
        $this->logoutLogActivity();
        return redirect()->route('home.login');
    }


    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img('inverse')]);
    }

    public function actionVoteCandidate(Request $request)
    {
        $student_id = auth()->guard('voter')->user()->student->id;
        $token = $this->tokenService->getByStudent($student_id);
        if ($this->checkIfNotExpired($token->use_time) == false) {
            $this->notValid();
            return response()->json('expired', 200);
        } else {
            $encrypted = Crypt::encryptString((int)$request->candidate_id);
            $encrypted = $this->encrypted->encryptVote($encrypted);
            $vote = $this->studentService->votingAction($student_id, $encrypted);
            // event(new QuickCountUpdated('vote'));
            return response()->json('success', 200);
        }
    }

    public function notValid()
    {
        $id = auth()->guard('voter')->user()->student->id;
        $encrypted = Crypt::encryptString(0);
        $vote = $this->studentService->votingAction($id, $this->encrypted->encryptVote($encrypted));

        Auth::guard('voter')->logout();
        return redirect()->route('vote.success');
    }

    public function checkIfNotExpired(string $time): bool
    {
        // check if time is 10 minutes ago (yyyy-mm-dd hh:mm:ss)
        $time = strtotime($time);
        $now = strtotime(date('Y-m-d H:i:s'));
        $diff = $now - $time;
        // check if diff is less than 10 minutes
        if ($diff < 600) {
            return true;
        } else {
            return false;
        }
    }

    public function test()
    {

        $faker = Faker::create('id_ID');
        $encrypted = Crypt::encryptString($faker->numberBetween(1, 2));

        $job = new votingJob(1, $encrypted, $this->studentService);
        $this->dispatch($job);
    }
}
