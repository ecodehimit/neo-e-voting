<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusServer;
use App\Services\Encrypter\EncrypterService;
use Illuminate\Support\Facades\DB;

class ServerController extends Controller
{
    protected $decrypted;

    public function __construct(EncrypterService $decrypted)
    {
        $this->decrypted = $decrypted;
    }


    public function index()
    {
        return view('admin.contents.servers.index', [
            'data' => StatusServer::first()
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'status' => 'required',
            'status' => 'in:0,1',
        ], [
            'status.required' => 'Status tidak boleh kosong',
            'status.in' => 'Status tidak valid',
        ]);
        $data = StatusServer::first();
        $data->status = $request->status;
        $data->save();
        return response()->json('success', 200);
    }

    public function testing()
    {
        $db = DB::table('calculations')->select('candidate_choice')->get();
        // foreach
        $data = $this->decrypted->decryptVote('Sc5jDERAtaIsDY7viBGqkvFjeF0hljT2tcZVF6s9A0cIlLe0/DO9mP9S2sQUQFZ0tqn3JJM8v8eF0AQSY6OoYqvLqW5VwEruY7aB30UPpKyH3gkLOuQK57yIEK41lidMc5b08LMbRQdZIJHH23RaoGMWcN9szjP2Ig7ecGz3sP5Y1+hnribihWTFCf/yaCcc3IdBY0s50abGZfrDCV5KUOXqTpgrlQDXDAgPDBAeY8s0FUcsqCGG8SvxTrSuGqVKQz2V1uMS+SIaumGbC6zWNZ+8nx9IDixANNGEG9F+t/k=');
        return $data;
    }
}
