<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $maxAttempts = 3; // Default is 5
    protected $decayMinutes = 2; // Default is 1

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->checkRoles();
    }
    public function username()
    {
        return 'nrp';
    }

    public function redirectTo()
    {
        // dd(Auth::user()->role);
        // if (Auth::user()->role == 'admin') {
        //     $this->redirectTo = RouteServiceProvider::ADMIN;
        //     return $this->redirectTo;
        // } elseif (Auth::user()->role == 'saksi') {
        //     $this->redirectTo = RouteServiceProvider::SAKSI;
        //     return $this->redirectTo;
        // } elseif (Auth::user()->role == 'panwaslu') {
        //     $this->redirectTo = RouteServiceProvider::PANWASLU;
        //     return $this->redirectTo;
        // } elseif (Auth::user()->role == 'gentoken') {
        //     $this->redirectTo = RouteServiceProvider::GENTOKEN;
        //     return $this->redirectTo;
        // }
        activity()
            ->causedBy(Auth::id())
            ->event('login admin')
            ->log('login admin');
        return $this->redirectTo = RouteServiceProvider::PANWASLU;
    }

    protected function logout(Request $request)
    {
        activity()
            ->causedBy(Auth::id())
            ->event('logout admin')
            ->log('logout admin');
        return view('auth.login');
    }
}
