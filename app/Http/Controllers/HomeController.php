<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Calculation;
use App\Repositories\Candidate\CandidateRepository;
use App\Services\Calculation\CalculationService;
use App\Services\Group\GroupService;
use App\Services\Student\StudentService;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public $studentService, $candidataRepository, $userService, $calculationService;

    public function __construct(StudentService $studentService, CandidateRepository $candidataRepository, UserService $userService, CalculationService $calculationService)
    {
        $this->studentService = $studentService;
        $this->userService = $userService;
        $this->candidataRepository = $candidataRepository;
        $this->calculationService = $calculationService;
        $this->middleware('auth');
    }

    public static function getLoginStatusByUserId($userId)
    {
        $login = Activity::where([
            'description' => 'login admin',
            'causer_id' => $userId,
        ])->orderBy('updated_at', 'DESC')->first();

        $logout = Activity::where([
            'description' => 'logout admin',
            'causer_id' => $userId,
        ])->orderBy('updated_at', 'DESC')->first();

        $date = new DateTime;
        $date->modify('-15 minutes');
        $carbonDate = Carbon::instance($date);

        if (!$login) return 'Belum';

        if ($logout == null) {
            $isNotLogout = true;
        } else {
            $isNotLogout = $login->updated_at->greaterThan($logout->updated_at);
        }

        if ($isNotLogout && $carbonDate->lessThanOrEqualTo($login->updated_at)) {
            return 'Sudah';
        } else {
            return 'Belum';
        }
    }

    public function cardSaksi()
    {
        return view('admin.contents.card.saksi');
    }

    public function index()
    {

        $dpt = $this->studentService->count(null);
        $sudah = $this->studentService->count('yes');
        $belum = $this->studentService->count('no');
        $candidate = $this->candidataRepository->count(null);
        $saksi = $this->userService->getSaksiList();
        $jumlahSaksi = 2; // Todo: ubah jumlah saksi sesuai dengan data

        view()->share([
            'dpt' => $dpt,
            'sudah' => $sudah,
            'belum' => $belum,
            'candidate' => $candidate,
            'saksi' => $saksi,
            'jumlahSaksi' => $jumlahSaksi,
            'saksiLengkap' => 0,
        ]);
        return view('admin.contents.dashboard');
        //        return view('layouts.admin');
    }

    public function totalPerangkatan()
    {
        if (request()->ajax()) {
            $data = $this->studentService->totalPerangkatan();
            return response()->json($data);
        }
        return view('admin.contents.totalPerangkatan');
    }
    public function quickCount()
    {
        $data = $this->calculationService->getDataVote();
        return response()->json($data, 200);
    }
}
