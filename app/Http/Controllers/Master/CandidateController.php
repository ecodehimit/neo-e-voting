<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\CandidateRequest;
use App\Services\Candidate\CandidateService;
use App\Services\Student\StudentService;
use Illuminate\Http\Request;

class CandidateController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $candidateService;
    public $studentService;

    public function __construct(CandidateService $candidateService, StudentService $studentService)
    {
        $this->candidateService = $candidateService;
        $this->studentService = $studentService;
    }
    public function index(Request $request)
    {
        if($request->ajax()) {
            $dataTabel = $this->candidateService->getDataTable(null); // (Y/T)
//            dd($dataTabel);
            return $dataTabel;
        }

        return view('admin.contents.candidate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = null;
        $students = $this->studentService->all();
        view()->share([
            'data' => $data,
            'students' => $students
        ]);
        return view('admin.contents.candidate.modal');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CandidateRequest $request)
    {
        $data = $this->candidateService->store($request);
        // dd($data);

        return response()->json('success',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->candidateService->find($id);
        $students = $this->studentService->all();

        view()->share([
            'data' => $data,
            'students' => $students,
        ]);

        return view('admin.contents.candidate.modal');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->candidateService->delete($id);
        return response()->json('success',200);
        
    }
}
