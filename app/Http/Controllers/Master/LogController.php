<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Services\Log\LogService;

class LogController extends Controller
{

    protected $log;

    public function __construct(LogService $log)
    {
        $this->log = $log;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return $this->log->getDataTable(null);
        }
        return view('admin.contents.log.index');
    }
}
