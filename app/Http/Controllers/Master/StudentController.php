<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use App\Imports\StudentsImport;
use App\Services\Group\GroupService;
use App\Services\Student\StudentService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $studentService, $groupService;

    public function __construct(StudentService $studentService, GroupService $groupService)
    {
        $this->studentService = $studentService;
        $this->groupService = $groupService;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dataTabel = $this->studentService->getDataTable(null); // (Y/T)
            //            dd($dataTabel);
            return $dataTabel;
        }

        return view('admin.contents.students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        dd('dwq');

        $data = null;
        $groups = $this->groupService->all();


        view()->share([
            'data' => $data,
            'groups' => $groups
        ]);

        return view('admin.contents.students.modal');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $data = $this->studentService->store($request);
        //        dd($data);

        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = $this->studentService->find($id);
        $groups = $this->groupService->all();


        view()->share([
            'data' => $data,
            'groups' => $groups
        ]);

        return view('admin.contents.students.modal');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->studentService->delete($id);
        return response()->json('success', 200);
    }

    public function importExcel()
    {
        $data = null;
        $groups = $this->groupService->all();


        view()->share([
            'data' => $data,
            'groups' => $groups
        ]);

        return view('admin.contents.students.import');
    }

    public function import(Request $request)
    {
        Excel::import(new StudentsImport($this->groupService), $request->file);
        return response()->json('success', 200);
    }
}
