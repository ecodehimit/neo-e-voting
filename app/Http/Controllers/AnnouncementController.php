<?php

namespace App\Http\Controllers;

use App\Repositories\Candidate\CandidateRepository;
use App\Services\Student\StudentService;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\Services\Encrypter\EncrypterService;
use Illuminate\Support\Facades\Cache;

class AnnouncementController extends Controller
{
    public $studentService, $candidataRepository, $decrypted;

    public function __construct(StudentService $studentService, CandidateRepository $candidataRepository, EncrypterService $decrypted)
    {
        $this->studentService = $studentService;
        $this->candidataRepository = $candidataRepository;
        $this->decrypted = $decrypted;
        $this->middleware('auth');
    }

    public function index($saksi)
    {
        // TODO: jangan lupa uncommment pas production
        // if ($saksi < 2) {
        //     return abort(404);
        // }

        if (!Cache::has('announcement')) {
            return response()->redirectToRoute('dashboard.home');
        }

        return view('admin.contents.announcement', [
            'data' => Cache::get('announcement'),
        ]);
    }

    public function calculate()
    {
        $candidates = $this->candidataRepository->all();
        $suara = DB::table('calculations')->select('candidate_choice')->get();
        $encryptSuara = [];
        $decryptSuara = [];

        for ($i = 0; $i < $suara->count(); $i++) {
            $encryptSuara[$i] = $suara[$i]->candidate_choice;
            $decryptSuara[$i] = $this->decrypted->decryptVote($encryptSuara[$i]);
            $decryptSuara[$i] = Crypt::decryptString($decryptSuara[$i]);
        }
        
        $groupDecryptSuara = array_count_values($decryptSuara);
        $kandidat = [];
        $i = 1;
        
        foreach ($candidates as $candidate) {
            // $kandidat[] = ($candidate->leader != null) ? $candidate->leader->student_name : 'Suara Kosong';
            
            if ($candidate->leader != NULL) {
                $kandidat[$i]['label'] = $candidate->leader->student_name . ' & ' . $candidate->vice->student_name;
            } else {
                $kandidat[$i]['label'] = 'Kotak Kosong';
            }

            $kandidat[$i]['nomer_paslon'] = $candidate->number;
            $kandidat[$i]['suara'] = array_key_exists($candidate->id, $groupDecryptSuara) ? $groupDecryptSuara[$candidate->id] : 0;
            $i++;
        }

        $kandidat[$i] = [
            'label' => 'Suara Kosong',
            'nomer_paslon' => '0',
            'suara' => array_key_exists(0, $groupDecryptSuara) ? $groupDecryptSuara[0] : 0,
        ];

        $groupCandidate = json_encode($kandidat, JSON_NUMERIC_CHECK);

        Cache::put('announcement', $groupCandidate, now()->addMinutes(30));

        return response()->redirectToRoute('dashboard.home');
    }

    public function groupVoteCandidate($candidates, $groupDecryptSuara)
    {
        $candidates_suara = [];
        $candidates_label = [];

        foreach ($candidates as $candidate) {
            $label = ($candidate->candidate_lead != null) ? $candidate->leader->student_name : 'Suara Kosong';
            $nomer = $candidate->number;
            $candidates_label[$nomer] = $label;
        }

        foreach ($groupDecryptSuara as $key => $value) {
            if (array_key_exists($key, $candidates_label)) {
                $candidates_suara[$candidates_label[$key]] = $value;
            }
        }

        // dd($candidates_suara, $groupDecryptSuara);
        return json_encode($candidates_suara, JSON_NUMERIC_CHECK);
    }
}
