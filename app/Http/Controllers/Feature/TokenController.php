<?php

namespace App\Http\Controllers\Feature;

use App\Http\Controllers\Controller;
use App\Repositories\Student\StudentRepository;
use App\Services\Token\TokenService;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public $tokenService, $studentRepository;
    public function __construct(TokenService $tokenService, StudentRepository $studentRepository)
    {
        $this->tokenService = $tokenService;
        $this->studentRepository = $studentRepository;
    }

    public function index(Request $request)
    {



        if ($request->ajax()) {
            $dataTabel = $this->tokenService->getDataTable(null); // (Y/T)
            //            dd($dataTabel);
            return $dataTabel;
        }

        return view('admin.contents.tokens.index');
    }

    public function update(Request $request)
    {

        $data = $this->tokenService->updateToken($request->id);

        return response()->json('success', 200);
    }

    public function autocomplete(Request $request)
    {
        $data = [];

        if ($request->filled('q')) {
            $data = $this->studentRepository->search($request->get('q'));
        }

        return response()->json($data);
    }

    public function store(Request $request)
    {

        $data = $this->tokenService->store($request);
        // return redirect()->back()->with(($data['status']) ? 'success' : 'error', $data['message']);
        return response()->json([
            'status' => $data['status'],
            'message' => $data['message']
        ], ($data['status']) ? 200 : 400);
    }

    public function regenerate(Request $request)
    {
        $data = $this->tokenService->regenerate($request->id);

        return response()->json([
            'status' => $data['status'],
            'message' => $data['message']
        ], ($data['status']) ? 200 : 400);

        //        return response()->json('success',200);
    }
}
