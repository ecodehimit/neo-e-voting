<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'candidate_lead' => 'required|numeric|unique:candidates,candidate_lead,' . $this->data_id,
            'candidate_vice' => 'required|numeric|unique:candidates,candidate_vice,' . $this->data_id,
            'number' => 'required|numeric|unique:candidates,number,' . $this->data_id,
            'photo' => 'image|max:1024',
            'visi' => 'required',
            'mission' => 'required',
        ];
    }
}
