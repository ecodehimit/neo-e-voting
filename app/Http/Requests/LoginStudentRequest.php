<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nrp' => ['required'],
            'token' => ['required'],
            'g-recaptcha-response' => 'recaptcha',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */

    public function messages()
    {
        return [
            'nrp.required' => 'NRP tidak boleh kosong',
            'token.required' => 'Token tidak boleh kosong',
            'g-recaptcha-response.recaptcha' => 'Captcha tidak valid',
        ];
    }
}
