<?php

namespace App\Http\Livewire;

use App\Models\Calculation;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class TimeStatsChart extends Component
{
    public function render()
    {
        // $calculation = DB::table('calculations')
        //     ->orderByRaw('TIME(created_at)')
        //     ->get();

        $calculation = Calculation::select('created_at')
            ->orderByRaw('TIME(created_at)')
            ->get();
        return view('livewire.time-stats-chart', [
            'calculation' => $calculation,
        ]);
    }
}
