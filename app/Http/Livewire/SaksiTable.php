<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Cache;

class SaksiTable extends Component
{

    private $userService;

    public function render(UserService $userService)
    {
        $this->userService = $userService;

        return view('livewire.saksi-table', [
            'saksi' => $this->userService->getSaksiList(),
            'jumlahSaksi' => 2,
            'kalkulasi' => Cache::has('announcement'),
        ]);
    }
}
