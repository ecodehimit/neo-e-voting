<?php

namespace App\Imports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithUpserts;
use App\Services\Group\GroupService;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentsImport implements ToModel, WithUpserts, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public $groupService;

    public function __construct(GroupService $groupService)
    {
        $this->groupService = $groupService;
    }

    public function model(array $row)
    {
        $keys = array_keys($row);

        // Format Excel
        // 2023, John Doe, 3123600001, D4 IT A

        // avoid reading empty row
        if (!isset($row[$keys[0]])) {
            return null;
        }

        // sometimes the "$row['kelas'] . ' ' . $row['tahun']"
        // is 'D4 IT A  2xxx'
        // or 'D4 IT A 2xxx'
        // so, i need remove those double space
        $concat = explode(' ', $row[$keys[3]] . ' ' . $row[$keys[0]]);

        // removing empty string
        $filteringGrade = array_filter($concat, function ($item) { return $item && 1; });

        $groupName = implode(' ', $filteringGrade);

        return new Student([
            'student_name' => $row[$keys[1]],
            'student_nrp' => strval($row[$keys[2]]),
            'group_id' => $this->groupService->searchIdByGroupName($groupName),
            'status' => 'no',
        ]);
    }

    public function uniqueBy()
    {
        return 'student_nrp';
    }
}
