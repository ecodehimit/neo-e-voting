<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Master\UserController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Master\LogController;
use App\Http\Controllers\ServerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', [\App\Http\Controllers\StudentController::class, 'test'])->name('test');
Route::get('/reload-captcha', [App\Http\Controllers\StudentController::class, 'reloadCaptcha']);
Route::get('vote-off', function () {
    return view('vote-off');
})->name('vote-off');
Route::get('/', function () {
    return view('index');
})->name('index')->middleware('status-server');
Route::get('/login-student', [\App\Http\Controllers\StudentController::class, 'index'])->name('home.login')->middleware('status-server');
Route::post('/action/loginStudent', [\App\Http\Controllers\StudentController::class, 'actionLogin'])->name('actionLogin.student')->middleware('status-server');

Route::get('/vote-success', [\App\Http\Controllers\StudentController::class, 'vote_succes'])->name('vote.success')->middleware('status-server');
Route::middleware(['auth:voter', 'status-server'])->group(function () {
    Route::get('/vote', [\App\Http\Controllers\StudentController::class, 'vote'])->name('vote');
    Route::post('/action/voteCandidate', [\App\Http\Controllers\StudentController::class, 'actionVoteCandidate'])->name('actionVoteCandidate');
    Route::get('/logout', [\App\Http\Controllers\StudentController::class, 'logout'])->name('voter.logout');
    Route::get('/vote/not-valid', [\App\Http\Controllers\StudentController::class, 'notValid'])->name('vote.notValid');
});


Auth::routes([
    'register' => false
]);

Route::get('/nowDate', [\App\Http\Controllers\HomeController::class, 'nowDate'])->name('nowDate');

Route::middleware(['auth'])->group(function () {
});

Route::middleware(['auth:web'])->group(function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard.home');

    Route::get('/announcement/calculate', [App\Http\Controllers\AnnouncementController::class, 'calculate'])->name('dashboard.announcement.calculate')->middleware('checkRole:admin');
    Route::get('/announcement/{saksiLengkap}', [App\Http\Controllers\AnnouncementController::class, 'index'])->name('dashboard.announcement')->middleware('checkRole:admin');

    // Master Student Routing
    Route::prefix('master')->name('master.')->group(function () {
        Route::resource('tokens', \App\Http\Controllers\Feature\TokenController::class);
        Route::get('/students/delete/{id}', [\App\Http\Controllers\Master\StudentController::class, 'destroy'])->name('students.delete')->middleware('checkRole:admin');
        Route::resource('candidate', \App\Http\Controllers\Master\CandidateController::class)->middleware('checkRole:admin');
        Route::get('/candidate/delete/{id}', [\App\Http\Controllers\Master\CandidateController::class, 'destroy'])->name('candidate.delete')->middleware('checkRole:admin');
        Route::get('/students/importExcel', [\App\Http\Controllers\Master\StudentController::class, 'importExcel'])->name('students.importExcel')->middleware('checkRole:admin');
        Route::post('/students/import', [\App\Http\Controllers\Master\StudentController::class, 'import'])->name('students.import')->middleware('checkRole:admin');
        Route::resource('students', \App\Http\Controllers\Master\StudentController::class)->middleware('checkRole:admin');
        Route::resource('candidate', \App\Http\Controllers\Master\CandidateController::class)->middleware('checkRole:admin');
        Route::resource('groups', \App\Http\Controllers\Master\GroupController::class)->middleware('checkRole:admin');
        Route::get('/groups/delete/{id}', [\App\Http\Controllers\Master\GroupController::class, 'destroy'])->name('groups.delete')->middleware('checkRole:admin');
        Route::resource('tokens', \App\Http\Controllers\Feature\TokenController::class)->middleware('checkRole:admin');
    });



    // Master Token Routing

    Route::prefix('feature')->name('feature.')->group(function () {
        Route::prefix('tokens')->name('tokens.')->group(function () {
            Route::get('/', [\App\Http\Controllers\Feature\TokenController::class, 'index'])->name('index')->middleware('checkRole:admin,token');
            Route::get('/updateToken', [\App\Http\Controllers\Feature\TokenController::class, 'update'])->name('update')->middleware('checkRole:admin,token');
            Route::post('/store', [\App\Http\Controllers\Feature\TokenController::class, 'store'])->name('store')->middleware('checkRole:admin,token');
            Route::get('/autocomplete', [\App\Http\Controllers\Feature\TokenController::class, 'autocomplete'])->name('autocomplete')->middleware('checkRole:admin,token');
            Route::get('/regenerate', [\App\Http\Controllers\Feature\TokenController::class, 'regenerate'])->name('regenerate')->middleware('checkRole:admin,token');
        });
    });


    // User Manager Routing
    Route::group(['prefix' => 'user-manager', 'middleware' => 'checkRole:admin'], function () {
        Route::get('/', [UserController::class, 'index'])->name('user-manager.index');
        Route::post('/store', [UserController::class, 'store'])->name('user-manager.store');
        Route::get('/create', [UserController::class, 'create'])->name('user-manager.create');
        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('user-manager.edit');
        Route::post('/update/{id}', [UserController::class, 'update'])->name('user-manager.update');
        Route::get('/delete/{id}', [UserController::class, 'destroy'])->name('user-manager.delete');
    });

    // Logs Routing
    Route::group(['prefix' => 'logs', 'middleware' => 'checkRole:admin'], function () {
        Route::get('/', [LogController::class, 'index'])->name('logs.index');
    });

    // server status
    Route::get('/server-status', [\App\Http\Controllers\ServerController::class, 'index'])->name('serverStatus')->middleware('checkRole:admin');
    Route::post('/server-status', [\App\Http\Controllers\ServerController::class, 'update'])->middleware('checkRole:admin');

    Route::get('/total-perangkatan', [\App\Http\Controllers\HomeController::class, 'totalPerangkatan'])->name('totalPerangkatan')->middleware('checkRole:admin,panwaslu,saksi');
    Route::get('/quick-count', [HomeController::class, 'quickCount'])->name('quickCount')->middleware('checkRole:admin');
});

Route::get('/testing', [ServerController::class, 'testing']);
