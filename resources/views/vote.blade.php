<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('assets_softUI/img/evote_logo.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>NEO E-Voting </title>

    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');

        /* This pen */
        body {
            font-family: 'Poppins', sans-serif;
            text-rendering: optimizeLegibility;
            font-weight: initial;
        }

        .swal-footer {
            text-align: center;
        }

        .postcard {
            overflow: hidden;
        }

        .postcard_img {
            max-height: 100%;
            height: 350px;
            width: 100%;
            object-fit: cover;
            position: relative;
            object-position: center;
            transition: transform 0.3s ease;
        }

        .dropdown-item:hover {
            background-color: rgba(var(--bs-danger-rgb), var(--bs-bg-opacity)) !important;
        }

        .card-container {
            display: flex;
            align-content: stretch;
        }
        
        .card {
            width: 100%;
        }

        .card:hover .postcard_img {
            transform: scale(1.1);
        }

        .card_candidate {
            box-shadow: 7px 7px 0px 0px black;
            -webkit-box-shadow: 10px 10px 0px 0px black;
            -moz-box-shadow: 10px 10px 0px 0px black;
            border: 2px solid black;
        }

        .swal-padding {
            padding-top: 2rem;
            padding-bottom: 3rem;
        }

        .card-number {
            background: #F5DF1D;
            top: 40px;
            right: -10px
        }

        @media (max-width: 990px) {
            .card-container {
                gap: 20px !important;
            }

            .card-number {
                top: 20px;
                right: -5px;
                font-size: 0.8rem !important;
            }

            .postcard_img {
                height: 120px;
            }

            .card-title {
                font-size: 0.8rem !important;
            }

            .accordion-button,
            .accordion-body {
                padding: 8px;
            }

            .accordion-button,
            .accordion-collapse,
            .card-vote-button {
                font-size: 0.6rem !important;
            }
        }
    </style>



</head>

<body>

    {{-- @dd(auth()->guard('voter')->user()->token) --}}

    <nav class="navbar navbar-expand-lg sticky-top mb-5 px-0 px-sm-5 py-4 box-shadow">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="./assets_softUI/img/evote_logo.png" alt="Logo" width="30" class="d-inline-block align-text-top">
                NEO E-Voting
            </a>
            <button class="navbar-toggler burger-icon" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ auth()->guard('voter')->user()->student->student_name }} (
                            {{ auth()->guard('voter')->user()->student->student_nrp }} )
                        </a>
                        <ul class="dropdown-menu bg-danger">
                            <li><a class="dropdown-item text-white" href="{{ route('voter.logout') }}">Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="container">
        <div class="time-left mb-4">
            <h3>Sisa Waktu : <span id="countdown"></span></h3>
        </div>
        <div class="card-container">
            @foreach ($candidates as $candidate)
            <section class="card card_candidate">
                <div class="card-body text-center d-flex flex-column">
                    <div class="postcard">
                        <img src="{{ asset('storage/storage/assets_softUI/img/' . $candidate->photo) }}"
                            class="postcard_img rounded" alt="...">
                        <span class="card-number position-absolute translate-middle fs-3 rounded-circle badge text-black">
                            {{ $candidate->number }}
                            <span class="visually-hidden">unread messages</span>
                        </span>
                    </div>

                    <h1 class="card-title my-4 fs-3">{{ $candidate->leader == NULL ? "Kotak Kosong" : $candidate->leader->student_name }} <br> X <br>
                        {{ $candidate->vice == NULL ? "Kotak Kosong" : $candidate->vice->student_name }}
                    </h1>

                    <div class="accordion mt-2" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $candidate->id }}" aria-expanded="false" aria-controls="collapse{{ $candidate->id }}">
                                    Vision :
                                </button>
                            </h2>
                            <div id="collapse{{ $candidate->id }}" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="collapse{{ $candidate->id }}">
                                <div class="accordion-body">
                                    {{ $candidate->visi }}
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $candidate->id }}" aria-expanded="false" aria-controls="collapse{{ $candidate->id }}">
                                    Mission :
                                </button>
                            </h2>
                            <div id="collapse{{ $candidate->id }}" class="accordion-collapse collapse show" aria-labelledby="headingTwo" data-bs-parent="collapse{{ $candidate->id }}">
                                <div class="accordion-body">
                                    {{ $candidate->mission }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#" class="card-vote-button btn w-100 fs-5 btn-primary mt-4" id="btn-submit" name="btn-submit" onclick="submit({{ $candidate->id }})">
                        <i class="fas fa-play mr-2"></i> Vote now
                    </a>
                </div>
            </section>
            @endforeach
        </div>
    </div>

    <script src="./assets/theme.js"></script>
    <script>
        swal('Anda memiliki waktu 10 menit untuk memilih', {
            icon: "warning",
        });
        // set timeout 10 menit
        var expired = new Date("{{ $expired_time }}"); // yyyy-mm-dd hh:mm:ss

        var countDown = setInterval(function() {
            var now = new Date().getTime();
            var distance = expired - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.getElementById("countdown").innerHTML = minutes + "m " + seconds + "s ";
            console.log(minutes + "m " + seconds + "s ");
            if (distance < 0) {
                clearInterval(countDown);
                document.getElementById("countdown").innerHTML = "EXPIRED";
                swal('Waktu anda habis', {
                    icon: "warning",
                });
                window.location.href = "{{ route('vote.notValid') }}";
            }
        }, 1000);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function submit(candidate) {
            // alert(candidate)

            swal({
                title: "Are you sure select candidate " + candidate + " ?",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
            }).then(function(isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        data: {
                            candidate_id: candidate
                        },
                        url: "{{ route('actionVoteCandidate') }}",
                        type: 'POST',
                        dataType: 'json',
                        beforeSend: function() {
                            swal({
                                title: 'Pilihanmu sedang diproses...',
                                button: false,
                                className: 'swal-padding',
                                onOpen: () => {
                                    Swal.showLoading()
                                }
                            });
                        },
                        success: function(result) {
                            console.log(result);

                            swal(
                                'Success!',
                                'Success select candidate!',
                                'success'
                            )
                            setTimeout(function() {
                                window.location.href = "{{ route('vote.success') }}";
                            }, 1500)

                        },
                        error: function(result) {
                            console.log(result);
                        }
                    });

                } else {
                    swal('tidak jadi memilih');
                }
            });
        }
    </script>

</body>

</html>