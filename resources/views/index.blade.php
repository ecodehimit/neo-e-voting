<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome | Suksesi HIMIT PENS 2023</title>
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <link rel="stylesheet" href="{{ asset('assets/style.css') }}">
    <script src="https://kit.fontawesome.com/1d954ea888.js"></script>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg py-4 box-shadow">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="{{ asset('assets/img/himit.png') }}" alt="logo" width="40" height="36"><strong>eVote</strong></a>
            <button class="navbar-toggler burger-icon" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a class="nav-link active-link" aria-current="page" href="#"><strong>Home</strong></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="#countdown">Countdown</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="#about-us">About Us</a>
                    </li>
                </ul>
                <div class="dropdown theme-dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="{{ asset('assets/theme-icon/system-light-active.svg') }}" alt="" width="24" height="24" id="theme-parent-icon">
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <button class="dropdown-item theme-button" id="light" data-icon="sun">
                                <img src="{{ asset('assets/theme-icon/sun-light.svg') }}" alt="" width="20" height="20" class="me-2">
                                <span>Light</span>
                            </button>
                        </li>
                        <li>
                            <button class="dropdown-item theme-button" id="dark" data-icon="moon">
                                <img src="{{ asset('assets/theme-icon/moon-light.svg') }}" alt="" width="20" height="20" class="me-2">
                                <span>Dark</span>
                            </button>
                        </li>
                        <li>
                            <button class="dropdown-item theme-button" id="system" data-icon="system">
                                <img src="{{ asset('assets/theme-icon/system-light-active.svg') }}" alt="" width="20" height="20" class="me-2">
                                <span class="active-link">System</span>
                            </button>
                        </li>
                    </ul>
                </div>
                <a href="{{ route('home.login') }}" class="btn btn-primary px-5">Login</a>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Hero Section -->
    <div class="container mt-5" id="hero">
        <div class="row align-items-center">
            <div class="col-md-6 order-md-1">
                <h1 class="display-5 fw-bold">Selamat Datang di <span id="vote">eVote</span><br><span class="typer" id="main" data-words="Suksesi HIMIT PENS 2023" data-delay="100" data-deleteDelay="1500"></span>
                    <span class="cursor" data-owner="main"></span>
                </h1>
                <p class="my-3">Who is the next leader?</p>
                <a href="{{ route('home.login') }}" class="btn btn-primary mb-5" style="width: 50%;">Login</a>
            </div>
            <div class="col-md-6 order-md-2 order-sm-first order-first">
                <img src="{{ asset('assets/img/hero-himit.png') }}" class="img-fluid">
            </div>
        </div>
    </div>
    <!-- End Hero Section -->

    <!-- partner section -->
    <div class="container mt-5" id="partner">
        <div class="row justify-content-center text-center">
            <div class="col-md-3 col-sm-6 col-6">
                <a href="https://www.instagram.com/penseepis/" target="_blank">
                    <img src="{{ asset('assets/img/pens.png') }}" class="img-fluid grayscale-img-hover">
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-6">
                <a href="https://www.instagram.com/himitpens/" target="_blank">
                    <img src="{{ asset('assets/img/himit.png') }}" class="img-fluid grayscale-img-hover">
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-6" id="kpu-logo">
                <img src="{{ asset('assets/img/kpu.png') }}" class="img-fluid grayscale-img-hover">
            </div>
            <div class="col-md-3 col-sm-6 col-6" id="kpu-dark-logo">
                <img src="{{ asset('assets/img/kpu-dark-v2.png') }}" class="img-fluid grayscale-img-hover">
            </div>
            <div class="col-md-3 col-sm-6 col-6" id="suksesi-logo">
                <a href="https://www.instagram.com/suksesihimit" target="_blank">
                    <img src="{{ asset('assets/img/suksesi.png') }}" class="img-fluid grayscale-img-hover">
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-6" id="suksesi-dark-logo">
                <a href="https://www.instagram.com/suksesihimit" target="_blank">
                    <img src="{{ asset('assets/img/suksesi-dark-v2.png') }}" class="img-fluid grayscale-img-hover">
                </a>
            </div>
        </div>
    </div>
    <!-- end partner section -->


    <!-- countdown  -->
    <div id="countdown" class="mt-5 py-5">
        <div class="container">
            <div class="py-5 text-center">
                <h3>We Are Almost There</h3>
            </div>
            <div class="row justify-content-center text-center g-2 g-lg-3">
                <div class="col-md-2 col-sm-5 col-5">
                    <div class="card bg-transparent border-0">
                        <span class="">
                            <h5>Hari</h5>
                        </span>
                        <h1 id="day" class="pb-5"></h1>
                    </div>
                </div>
                <div class="col-md-1 col-sm-1 col-1">
                    <div class="card bg-transparent border-0">
                        <span class="">
                            <h5 class="opacity-0">-</h5>
                        </span>
                        <h1 id="day" class="pb-5">:</h1>
                    </div>
                </div>
                <div class="col-md-2 col-sm-5 col-5">
                    <div class="card bg-transparent border-0">
                        <span class="">
                            <h5>Jam</h5>
                        </span>
                        <h1 id="hour" class="pb-5"></h1>
                    </div>
                </div>
                <div class="col-md-1 col-sm-1 col-1 d-md-block d-none">
                    <div class="card bg-transparent border-0">
                        <span class="">
                            <h5 class="opacity-0">-</h5>
                        </span>
                        <h1 id="day" class="pb-5">:</h1>
                    </div>
                </div>
                <div class="col-md-2 col-sm-5 col-5">
                    <div class="card bg-transparent border-0">
                        <span class="">
                            <h5>Menit</h5>
                        </span>
                        <h1 id="minute" class="pb-5"></h1>
                    </div>
                </div>
                <div class="col-md-1 col-sm-1 col-1">
                    <div class="card bg-transparent border-0">
                        <span class="">
                            <h5 class="opacity-0">-</h5>
                        </span>
                        <h1 id="day" class="pb-5">:</h1>
                    </div>
                </div>
                <div class="col-md-2 col-sm-5 col-5">
                    <div class="card bg-transparent border-0">
                        <span class="">
                            <h5>Detik</h5>
                        </span>
                        <h1 id="second" class="pb-5"></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end countdown -->

    <!-- Start About Suksesi -->
    <section class="container pt-5" id="about-us">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h3 class="fw-bold">Apa itu <span class="text-primary">Suksesi HIMIT PENS 2023?</span></h3>
                <p class="mt-4">Suksesi HIMIT PENS 2023 adalah pemilihan umum yang diadakan guna menentukan penerus
                    ketua dan wakil ketua HIMIT PENS selama periode tahun 2023. Proses suksesi ini melibatkan
                    partisipasi aktif dari seluruh anggota HIMIT PENS, dimulai dari pencalonan, kampanye, hingga
                    pemungutan suara.</p>
                <p class="mt-1">Dengan adanya Suksesi HIMIT PENS 2023, diharapkan akan terpilih pemimpin yang mampu
                    membawa visi, inovasi, dan kemajuan bagi perkembangan serta kesejahteraan anggota HIMIT PENS.</p>
            </div>
            <div class="col-md-6">
                <img src="{{ asset('assets/img/about-suksesi.png') }}" class="img-fluid" style="max-height: 80%">
            </div>
        </div>

    </section>
    <!-- End About Suksesi -->

    <!-- Group -->
    <section class="container pt-5">
        <div class="row align-items-center">
            <div class="col-md-6">
                <img src="{{ asset('assets/img/group.png') }}" class="img-fluid" style="max-height: 80%">
            </div>
            <div class="col-md-6">
                <button class="btn btn-secondary text-start" type="button" data-bs-toggle="collapse" data-bs-target="#one" aria-expanded="false" aria-controls="one" style="width: 100%;">
                    <div class="container">
                        <div class="row">
                            <div class="col text-start"><strong>
                                    <h5 class="fw-bold">Apa itu E-CODE</h5>
                                </strong></div>
                            <div class="col text-end"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </button>
                <div class="collapse" id="one">
                    <div class="card card-body bg-primary text-white p-4">
                        E-CODE merupakan suatu organisasi mahasiswa dibawah naungan HIMIT PENS yang berkedudukan di
                        Departemen Teknik Informatika dan Komputer PENS.
                    </div>
                </div>
                <button class="btn btn-secondary text-start" type="button" data-bs-toggle="collapse" data-bs-target="#two" aria-expanded="false" aria-controls="two" style="width: 100%;">
                    <div class="container">
                        <div class="row">
                            <div class="col text-start"><strong>
                                    <h5 class="fw-bold">Apa itu eVote</h5>
                                </strong></div>
                            <div class="col text-end"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </button>
                <div class="collapse" id="two">
                    <div class="card card-body bg-primary text-white p-4">
                        eVote merupakan aplikasi berbasis website yang dikembangkan oleh E-CODE HIMIT PENS. Aplikasi ini
                        memungkinkan pengguna untuk melakukan polling dan kalkulasi perhitungan perolehan suara.
                    </div>
                </div>
                <button class="btn btn-secondary text-start" type="button" data-bs-toggle="collapse" data-bs-target="#three" aria-expanded="false" aria-controls="three" style="width: 100%;">
                    <div class="container">
                        <div class="row">
                            <div class="col text-start"><strong>
                                    <h5 class="fw-bold">Fungsional E-Code</h5>
                                </strong></div>
                            <div class="col text-end"><i class="fas fa-chevron-down"></i></div>
                        </div>
                    </div>
                </button>
                <div class="collapse" id="three">
                    <div class="card card-body bg-primary text-white p-4">
                        E-CODE merupakan organisasi yang berfungsi sebagai wadah penyalur kualitas sumber daya manusia
                        anggotanya.
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End Group -->
    <footer class="text-center py-4 container">
        <hr>
        <p>
            ©2023 eVote by E-CODE HIMIT PENS. Made with 💙 from Surabaya
        </p>
    </footer>

    <script src="./assets/script.js"></script>
    <script src="./assets/theme.js"></script>
    <script async src="https://unpkg.com/typer-dot-js@0.1.0/typer.js"></script>

</body>

</html>