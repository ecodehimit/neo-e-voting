<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $type;
    public $name;
    public $label;
    public $value;
    public $placeholder;
    public $required;
    public $validateName;
    public $accept;

    public function __construct($type,$name,$label,$value,$placeholder,$required,$validateName, $accept = null)
    {
        $this->type = $type;
        $this->accept = $accept;
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
        $this->placeholder = $placeholder;
        $this->required = $required;
        $this->validateName = $validateName;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
//        return view('components.input');
        return view('admin.components.input');
    }
}
