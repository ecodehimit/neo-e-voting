<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<!-- Nucleo Icons -->
{{--    <link href="../assets/css/nucleo-icons.css" rel="stylesheet" /> --}}
<link href="{{ asset('./assets_softUI/css/nucleo-icons.css') }}" rel="stylesheet" />
<link href="{{ asset('./assets_softUI/css/nucleo-svg.css') }}" rel="stylesheet" />
{{--    <link href="../assets/css/nucleo-svg.css" rel="stylesheet" /> --}}
<!-- Font Awesome Icons -->
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
{{--    <link href="../assets/css/nucleo-svg.css" rel="stylesheet" /> --}}
<link href="{{ asset('./assets_softUI/css/nucleo-svg.css') }}" rel="stylesheet" />

<!-- CSS Files -->
{{--    <link id="pagestyle" href="../assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" /> --}}
<link id="pagestyle" href="{{ asset('./assets_softUI/css/soft-ui-dashboard.css') }}" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


{{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.13.1/b-2.3.3/b-colvis-2.3.3/b-html5-2.3.3/b-print-2.3.3/cr-1.6.1/date-1.2.0/fc-4.2.1/fh-3.3.1/kt-2.8.0/r-2.4.0/rg-1.3.0/rr-1.3.1/sc-2.0.7/sb-1.4.0/sp-2.1.0/sl-1.5.0/sr-1.2.0/datatables.min.css"/> --}}

{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script> --}}
{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script> --}}
{{-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.13.1/b-2.3.3/b-colvis-2.3.3/b-html5-2.3.3/b-print-2.3.3/cr-1.6.1/date-1.2.0/fc-4.2.1/fh-3.3.1/kt-2.8.0/r-2.4.0/rg-1.3.0/rr-1.3.1/sc-2.0.7/sb-1.4.0/sp-2.1.0/sl-1.5.0/sr-1.2.0/datatables.min.js"></script> --}}


<style>
    .navbar-vertical .navbar-nav>.nav-item .nav-link.active .icon {
        background-color: #184677 !important;
        background-image: none;
    }

    .navbar-vertical.bg-white .navbar-nav .nav-link .icon {
        background-image: none;
    }

    .bg-gradient-primary {
        background-color: #184677 !important;
        background-image: none;
        /*background-image: linear-gradient(310deg, #f7e225 0%, #184677  50%);*/
    }

    .btn-warning {
        background-color: #f7e225;
        color: #184677;
    }

    .btn-primary {
        background-color: #184677;
    }

    .page-link.active,
    .active>.page-link {
        background-color: #184677;
        border-color: white;
    }
</style>
