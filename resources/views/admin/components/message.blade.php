@if ($message = Session::get('success'))

<div class="alert alert-success alert-dismissible" role="alert">
    <p class="mb-0"><a class="alert-link" href="javascript:void(0)">Success </a> {{ $message }}</p>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

@endif

@if ($message = Session::get('danger'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <p class="mb-0"><a class="alert-link" href="javascript:void(0)">Danger </a> {{ $message }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-dismissible" role="alert">
        <p class="mb-0"><a class="alert-link" href="javascript:void(0)">Warning </a> {{ $message }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

@if ($message = Session::get('info'))
    <div class="alert alert-info alert-dismissible" role="alert">
        <p class="mb-0"><a class="alert-link" href="javascript:void(0)">Info </a> {{ $message }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br />
@endif
