<script src="{{ asset('./assets_softUI/js/core/popper.min.js') }}"></script>
<script src="{{ asset('./assets_softUI/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('./assets_softUI/js/plugins/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('./assets_softUI/js/plugins/smooth-scrollbar.min.js') }}"></script>
<script src="{{ asset('./assets_softUI/js/plugins/chartjs.min.js') }}"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('./assets_softUI/js/soft-ui-dashboard.min.js') }}"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
    src="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.13.1/b-2.3.3/b-colvis-2.3.3/b-html5-2.3.3/b-print-2.3.3/cr-1.6.1/date-1.2.0/fc-4.2.1/fh-3.3.1/kt-2.8.0/r-2.4.0/rg-1.3.0/rr-1.3.1/sc-2.0.7/sb-1.4.0/sp-2.1.0/sl-1.5.0/sr-1.2.0/datatables.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/luxon/3.2.1/luxon.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-streaming@2.0.0/dist/chartjs-plugin-streaming.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chartjs-adapter-luxon/0.2.1/chartjs-adapter-luxon.min.js"></script>
