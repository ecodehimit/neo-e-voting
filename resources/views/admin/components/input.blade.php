<div class="mb-4">
    @if($label)
    <label class="form-label" for="example-text-input">{{$label}}</label>
    @endif
    <div class="mb-4">
        <input type="{{$type}}" class="form-control @error($name) is-invalid @enderror" name="{{$name}}" id="{{$name}}"
            value="{{$value}}" accept="{{$accept}}" placeholder="{{$placeholder}}">
        <div style="color: red" id="{{$validateName}}" name="{{$validateName}}"></div>
    </div>
</div>
