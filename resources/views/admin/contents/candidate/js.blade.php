<script type="text/javascript">
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /* -------------- render datatables------------------------ */
        var table = $('#item').DataTable({
            language: {
                paginate: {
                    next: '<i class="fa fa-fw fa-long-arrow-right">',
                    previous: '<i class="fa fa-fw fa-long-arrow-left">'  
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('master.candidate.index') }}",
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'number',
                    name: 'number'
                },
                {
                    data: 'candidate_lead',
                    name: 'leader.student_name'
                },
                {
                    data: 'candidate_vice',
                    name: 'vice.student_name'
                },
                {
                    data: 'photo',
                    name: 'photo',
                },
                {
                    data: 'visi',
                    name: 'visi',
                },
                {
                    data: 'mission',
                    name: 'mission',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                },
            ]
        });

        window.refreshTable = function() {
            table.draw();
        }

        /* --------------  modal create------------------------ */
        $('#refresh').click(function() {
            window.parent.refreshTable();
        });

        $('#createNewProduct').click(function() {
            $('#itemModal').load('{{ route('master.candidate.create') }}');

            $('#ajaxModel').modal('show');
        });

        /* --------------  modal edit------------------------ */
        $('body').on('click', '.editProduct', function() {
            var data_id = $(this).data('id');
            $('#itemModal').load('{{ route('master.candidate.index') }}' + '/' + data_id + '/edit');
            $('#ajaxModel').modal('show');
        });

        /*------------------------------------------
       --------------------------------------------
       Delete Product Code
       --------------------------------------------
       --------------------------------------------*/
        $('body').on('click', '.deleteProduct', function() {

            var data_id = $(this).data('id');
            swal({
                title: "Are you sure?",
                // text: "",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    swal({
                        title: 'Success!',
                        text: 'you have deleted data!',
                        icon: 'success'
                    }).then(function() {

                        $.ajax({
                            type: "get",
                            url: "{{ route('master.candidate.index') }}" +
                                '/delete/' + data_id,
                            success: function(data) {
                                table.draw();
                            },
                            error: function(data) {
                                console.log('Error:', data.responseText);
                            }
                        });

                    });
                } else {
                    swal("Cancelled", "you cancel the action :)", "error");
                }
            })


        });

    });
</script>
