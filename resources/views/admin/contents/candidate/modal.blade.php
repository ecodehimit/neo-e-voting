<div class="alert alert-danger" style="display:none"></div>

<div class="modal-header" style="background-color: #2b3b98">
    <h4 class="modal-title text-white" id="modelHeading">Form</h4>
    <div class="block-options text-white">
        <a class="btn-block-option text-white" data-bs-dismiss="modal" aria-label="Close"
            style="background-color: transparent !important;">
            <i class="fa fa-fw fa-times"></i>
        </a>
    </div>
</div>

{{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque itaque labore saepe quia reiciendis, ullam a nobis ex officia distinctio. --}}
<form id="productForm" name="productForm" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input type="text" value="{{ $data ? $data->id : '' }}" name="data_id" id="data_id"
            style="  visibility: hidden;">
        <div class="row">
            <div class="row push p-2">
                <div class="col-lg-12 col-xl-6">
                    <x-input type="text" name="number" label="Candidate Number"
                        value="{{ $data ? $data->number : '' }}" placeholder="candidate_number" required=""
                        validateName="validate_number"></x-input>
                    <div class="mb-4">
                        <label for="select2Multiple">Candidate Leader </label>
                        <div class="mb-4">
                            <select class="select2 form-control" name="candidate_lead" id="select2Lead">
                                @if ($data)
                                    <option value="{{ $data->candidate_lead }}" selected>
                                        {{ $data->leader == NULL ? "Kotak Kosong" : $data->leader->student_name }}
                                    </option>
                                @endif
                                @foreach ($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->student_name }}</option>
                                @endforeach
                            </select>
                            <div style="color: red" id="validate_candidate_lead" name="validate_candidate_lead"></div>

                        </div>
                    </div>
                    <div class="mb-4">
                        <label for="select2Multiple">Candidate Vice </label>
                        <div class="mb-4">
                            <select class="select2 form-control" name="candidate_vice" id="select2Vice">
                                @if ($data)
                                    <option value="{{ $data->candidate_vice }}" selected>{{ $data->vice->student_name }}
                                    </option>
                                @endif
                                @foreach ($students as $student)
                                    <option value="{{ $student->id }}">{{ $student->student_name }}</option>
                                @endforeach
                            </select>
                            <div style="color: red" id="validate_candidate_vice" name="validate_candidate_vice"></div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-6">
                    <div class="mb-4">
                        <label for="visi">Vision</label>
                        <textarea class="form-control" name="visi" id="visi" cols="30" rows="1"
                            validateName="validate_vision" required="" placeholder="Vision">{{ $data ? $data->visi : '' }}</textarea>
                    </div>
                    <div class="mb-4">
                        <label for="mission">Mission</label>
                        <textarea class="form-control" name="mission" id="mission" cols="30" rows="1"
                            validateName="validate_mission" required="" placeholder="Mission">{{ $data ? $data->mission : '' }}</textarea>
                    </div>

                    <div class="mb-4">
                        <label class="form-label" for="example-text-input">Photo</label>
                        <img src="{{ $data ? asset('storage/storage/assets_softUI/img/' . $data->photo) : '' }}"
                            class="img-preview img-fluid mb-3 col-sm-5 ">
                        <div class="mb-4">
                            <input type="file" class="form-control" name="photo" id="photo" value=""
                                placeholder="Photo of Candidate" onchange="previewImage()">
                            <div style="color: red" id="validate_photo" name="validate_photo"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="block-content block-content-full text-end bg-body border-footer-modal">
            {{--        <a id="saveBtn"  class="btn btn-sm btn-primary">Save</a> --}}
            <button type="submit" id="saveBtn" class="btn btn-sm btn-success">Save</button>
            <a class="btn btn-sm btn-alt-secondary" data-bs-dismiss="modal">Back</a>
        </div>
    </div>
</form>


<script type="text/javascript">
    // Preview Image 
    function previewImage() {
        var file = document.querySelector('input[type=file]').files[0];
        $('.img-preview').css('display', 'block')
        const reader = new FileReader();

        reader.addEventListener("load", () => {
            // preview.src = reader.result;
            $('.img-preview').attr('src', reader.result);
        }, false);

        // reader.onloadend = function(event) {
        //     $('.img-preview').attr('src', event.target.result);
        // }
        if (file) {
            reader.readAsDataURL(file);
        }
    }

    $(document).ready(function() {
        $('#select2Lead').select2({
            placeholder: "Select",
            allowClear: true
        });
        $('#select2Vice').select2({
            placeholder: "Select",
            allowClear: true
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveBtn').val("create-product");
        $('#productForm').trigger("reset");




        /*---------------------------------------------
           --------------------------------------------
           Action Items Create
           --------------------------------------------
           --------------------------------------------*/
        $('#saveBtn').on('click', function(e) {
            e.preventDefault();

            // var data = document.getElementById('productForm');
            var formData = new FormData($('#productForm')[0]);
            // console.log(formData);
            $.ajax({
                data: formData,
                type: 'POST',
                url: '{{ route('master.candidate.store') }}',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(result) {
                    // console.log(result);
                    swal(
                        'Success!',
                        'You clicked the button!',
                        'success'
                    )
                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');

                    window.parent.refreshTable();
                },
                error: function(result) {
                    console.log(result.responseText);
                    if (result.responseJSON == undefined) {

                        $('#productForm').trigger("reset");
                        $('#ajaxModel').modal('hide');

                        window.parent.refreshTable();

                    } else {
                        var error = result.responseJSON.errors;

                        console.log(error);
                        //
                        if (error.number != undefined) {
                            $('#validate_number').html('*' + error.number[0]);
                        } else {
                            $('#validate_number').html('');
                        }
                        //
                        if (error.candidate_lead != undefined) {
                            $('#validate_candidate_lead').html('*' + error.candidate_lead[
                                0]);
                        } else {
                            $('#validate_candidate_lead').html('');
                        }
                        //
                        if (error.candidate_vice != undefined) {
                            $('#validate_candidate_vice').html('*' + error.candidate_vice[
                                0]);
                        } else {
                            $('#validate_candidate_vice').html('');
                        }
                        //
                        if (error.visi != undefined) {
                            $('#validate_vision').html('*' + error.visi[0]);
                        } else {
                            $('#validate_vision').html('');
                        }
                        //
                        if (error.mission != undefined) {
                            $('#validate_mission').html('*' + error.mission[0]);
                        } else {
                            $('#validate_mission').html('');
                        }
                        //
                        if (error.photo != undefined) {
                            $('#validate_photo').html('*' + error.photo[0]);
                        } else {
                            $('#validate_photo').html('');
                        }
                    }
                }
            })
        });
    })
</script>
