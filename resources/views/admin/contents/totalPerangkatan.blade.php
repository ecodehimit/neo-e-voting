@extends('layouts.admin')
@section('breadcrumb1', 'Page')
@section('title_page', 'Dashboard')

@section('content')

@push('style')
@endpush

@push('script')
<script>
    const chartContainer = document.getElementById('chart-container');

    function createChart(year, data) {
        const chartDiv = document.createElement('div');
        chartDiv.classList.add('col-md-6');
        chartDiv.innerHTML = `
            <div class="card-body p-3">
                <div class="chart">
                    <h5 class="text-center">Angkatan ${year}</h5>
                    <canvas id="chart${year}" width="400" height="400"></canvas>
                </div>
            </div>
        `;

        const chart = chartDiv.querySelector('canvas').getContext('2d');
        new Chart(chart, {
            type: 'bar',
            data: {
                labels: Object.keys(data),
                datasets: [{
                    label: 'Sudah Memilih',
                    data: Object.values(data).map(item => item.yes),
                    borderWidth: 1,
                    backgroundColor: [
                        '#FF8EA6',
                        '#FFB163',
                        '#FFDC87',
                        '#64C9C9',
                        '#91CEF6',
                        '#BD9BFF',
                    ]
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        chartContainer.prepend(chartDiv);
    }

    function renderChart() {
        $.ajax({
            url: "{{ route('totalPerangkatan') }}",
            type: "GET",
            dataType: "json",
            success: function(data) {
                let finalData = {};
                for (let i = 0; i < data.length; i++) {
                    let group = data[i].group.group_name;
                    let year = group.substring(group.length - 4);
                    let major = group.substring(0, group.length - 5);

                    if (!finalData[`${year}`]) {
                        finalData[`${year}`] = {};
                    }

                    if (!finalData[`${year}`][`${major}`]) {
                        finalData[`${year}`][`${major}`] = {};
                        finalData[`${year}`][`${major}`]['yes'] = 0;
                        finalData[`${year}`][`${major}`]['no'] = 0;
                    }

                    if (data[i].status == 'yes') {
                        finalData[`${year}`][`${major}`]['yes'] += data[i].total;
                    } else {
                        finalData[`${year}`][`${major}`]['no'] += data[i].total;
                    }
                }

                console.log(finalData);

                for (const [key, value] of Object.entries(finalData)) {
                    createChart(key, value);
                }
            }
        })
    }

    renderChart();
</script>

<script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<!-- <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }

    if ("{{ Auth::user()->role }}" == 'admin') {
        const chart_quick_count = document.getElementById('chart_quick_count').getContext('2d');
        let chart_instance;

        async function createChartQuick(canvas, data) {
            const arr_data = Object.values(data);
            const arr_candidate_choice = arr_data.map((calculation) => calculation.candidate_name);
            
            const arr_data_total = arr_data.map((calculation) => calculation.total);

            chart_instance = new Chart(canvas, {
                type: 'doughnut',
                data: {
                    labels: arr_candidate_choice,
                    datasets: [{
                        label: 'Jumlah Suara Sementara',
                        data: arr_data_total,
                        borderWidth: 1,
                        backgroundColor: [
                            '#3A416F',
                            '#F87171',
                            '#640D6B',
                            '#0E46A3',
                        ]
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            return chart_instance;
        }

        function renderChartQuick() {
            $.ajax({
                url: "{{ route('quickCount') }}",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    if (chart_instance) {
                        chart_instance.destroy();
                    }
                    createChartQuick(chart_quick_count, data);
                }
            })
        }
        const pusher = new Pusher("{{ env('PUSHER_APP_KEY') }}", {
            cluster: 'ap1'
        });
        const channel = pusher.subscribe('quick-count-updated');
        channel.bind('quick-count-event', function(data) {
            renderChartQuick();
        });
        renderChartQuick();
    }
</script> -->
@endpush
<div class="row">
    <div class="col-lg-12">
        <div class="card z-index-2">
            <div class="card-header pb-0 d-flex justify-content-between">
                <h6>Tolok Ukur Keberhasilan</h6>
            </div>
            <div id="chart-container" class="row">
            </div>
        </div>
    </div>
</div>
<!-- <div class="row mt-4">
    @if (Auth::user()->role == 'admin')
    <div class="col-12">
        <div class="card z-index-2">
            <div class="card-header pb-0 d-flex justify-content-between">
                <h6>Quick Count</h6>
            </div>
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="card-body p-3">
                        <div class="chart">
                            <h5 class="text-center">Jumlah Suara Sementara</h5>
                            <canvas id="chart_quick_count" width="400" height="400"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div> -->
@endsection