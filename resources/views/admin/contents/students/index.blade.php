@extends('layouts.admin')

@section('breadcrumb1','Master')
@section('title_page','Students')

@section('content')

@push('style')

@include('admin.components.datatable')

@include('admin.components.sweetalert')

@include('admin.components.select2')

<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #1648d6;
    }

    .select2-close-mask {
        z-index: 2099;
    }

    .select2-dropdown {
        z-index: 3051;
    }

    .swal-footer {
        text-align: center;
    }

    .select2-selection__rendered {
        line-height: 34px !important;
    }

    .select2-container .select2-selection--single {
        height: 39px !important;
    }

    .select2-selection__arrow {
        height: 34px !important;
    }
</style>
@endpush



<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>List - Student</h6>
                <div class="d-flex justify-content-between">
                    <a class="btn btn-warning" href="javascript:void(0)" id="createNewProduct"> Create</a>
                    <a class="btn btn-warning" href="javascript:void(0)" id="importNewProduct"> Import</a>
                </div>

            </div>
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-3">
                    <table class="table align-items-center mb-0" id="item">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">#</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">NRP
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Name</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Class</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Status</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Action</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="ajaxModel" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="itemModal" name="itemModal"></div>
        </div>
    </div>

</div>

@push('script')


@endpush

@include('admin.contents.students.js')

@endsection