<div class="alert alert-danger" style="display:none"></div>

<div class="modal-header" style="background-color: #2b3b98">
    <h4 class="modal-title text-white" id="modelHeading">Form</h4>
    <div class="block-options text-white">
        <a class="btn-block-option text-white" data-bs-dismiss="modal" aria-label="Close" style="background-color: transparent !important;">
            <i class="fa fa-fw fa-times"></i>
        </a>
    </div>
</div>


<form id="productForm" name="productForm" class="form-horizontal">
    <div class="modal-body">
        <input type="text" value="{{($data) ? $data->id :''}}" name="data_id" id="data_id" style="  visibility: hidden;">
        <div class="row">
            <div class="row push p-2">
                <div class="col-lg-12 col-xl-6">
                    <x-input type="text" name="student_name" label="Student Name" value="{{($data) ? $data->student_name :''}}" placeholder="student_name" required="" validateName="validate_student_name"></x-input>
                    <x-input type="text" name="student_nrp" label="Student NRP" value="{{($data) ? $data->student_nrp :''}}" placeholder="student_nrp" required="" validateName="validate_student_nrp"></x-input>

                </div>
                <div class="col-lg-12 col-xl-6">

                    <div class="mb-4">
                        <label for="select2Multiple">Class </label>
                        <div class="mb-4">
                            <select class="select2 form-control" name="group_id" id="select2">
                                @if($data)
                                <option value="{{$data->group_id}}" selected>{{$data->group->group_name}}</option>
                                @endif
                                @foreach($groups as $group)
                                <option value="{{$group->id}}">{{$group->group_name}}</option>
                                @endforeach
                            </select>
                            <div style="color: red" id="validate_student_group" name="validate_student_group"></div>

                        </div>
                    </div>
                    <div class="mb-4">
                        <label for="select2Multiple">Status </label>
                        <div class="mb-4">
                            <select class="form-control" name="status">
                                <option value="no" {{($data) ? ($data->status == 'no') ? 'selected' : '' :''}}>No
                                </option>
                                <option value="yes" {{($data) ? ($data->status == 'yes') ? 'selected' : '' :''}}>Yes
                                </option>
                            </select>
                            <div style="color: red" id="validate_student_group" name="validate_student_group"></div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="block-content block-content-full text-end bg-body border-footer-modal">
            {{-- <a id="saveBtn" class="btn btn-sm btn-primary">Save</a>--}}
            <button type="button" id="saveBtn" class="btn btn-sm btn-success">Save</button>
            <a class="btn btn-sm btn-alt-secondary" data-bs-dismiss="modal">Back</a>
        </div>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: "Select",
            allowClear: true
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveBtn').val("create-product");
        $('#productForm').trigger("reset");



        /*---------------------------------------------
           --------------------------------------------
           Action Items
           --------------------------------------------
           --------------------------------------------*/
        $('#saveBtn').click(function(e) {
            e.preventDefault();

            $.ajax({
                data: $('#productForm').serialize(),
                url: "{{ route('master.students.store') }}",
                type: "POST",
                dataType: 'json',
                success: function(result) {
                    console.log(result);
                    swal(
                        'Success!',
                        'You clicked the button!',
                        'success'
                    )
                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');

                    window.parent.refreshTable();

                    // location.reload();

                },
                error: function(result) {
                    // console.log(result);
                    if (result.responseJSON == undefined) {

                        $('#productForm').trigger("reset");
                        $('#ajaxModel').modal('hide');

                        window.parent.refreshTable();

                    } else {
                        var error = result.responseJSON.errors;

                        console.log(error);
                        //
                        if (error.student_name != undefined) {
                            $('#validate_student_name').html('*' + error.student_name[0]);
                        } else {
                            $('#validate_student_name').html('');
                        }
                        //
                        if (error.student_nrp != undefined) {
                            $('#validate_student_nrp').html('*' + error.student_nrp[0]);
                        } else {
                            $('#validate_student_nrp').html('');
                        }
                        //
                        if (error.group_id != undefined) {
                            $('#validate_student_group').html('*' + error.group_id[0]);
                        } else {
                            $('#validate_student_group').html('');
                        }

                    }


                }
            });
        });


    });
</script>