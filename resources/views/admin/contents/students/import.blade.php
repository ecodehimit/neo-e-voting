<div class="alert alert-danger" style="display:none"></div>

<div class="modal-header" style="background-color: #2b3b98">
    <h4 class="modal-title text-white" id="modelHeading">Form</h4>
    <div class="block-options text-white">
        <a class="btn-block-option text-white" data-bs-dismiss="modal" aria-label="Close"
            style="background-color: transparent !important;">
            <i class="fa fa-fw fa-times"></i>
        </a>
    </div>
</div>


<form id="productForm" name="productForm" class="form-horizontal" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        <div class="row">
            <div class="row push p-2">
                <div class="col-lg-12 col-xl-12">
                    <x-input
                        type="file" name="file" label="Students List File" value="" placeholder="file"
                        required="" id="file" validateName="validate_student"
                        accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                        ></x-input>
                </div>
            </div>
        </div>
        <div class="block-content block-content-full text-end bg-body border-footer-modal">
            {{-- <a id="saveBtn" class="btn btn-sm btn-primary">Save</a> --}}
            <button type="button" id="saveBtn" class="btn btn-sm btn-success">Save</button>
            <a class="btn btn-sm btn-alt-secondary" data-bs-dismiss="modal">Back</a>
        </div>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: "Select",
            allowClear: true
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveBtn').val("import-product");
        $('#productForm').trigger("reset");



        /*---------------------------------------------
           --------------------------------------------
           Action Items
           --------------------------------------------
           --------------------------------------------*/
        $('#saveBtn').click(function(e) {
            e.preventDefault();
            const file = $('#productForm')[0][0]['files'][0];

            var form_data = new FormData();
            form_data.append('file', file);

            for (var pair of form_data.entries()) {
                console.log(pair[0] + ', ' + pair[1]);
            }

            $.ajax({
                url: "{{ route('master.students.import') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'POST',
                dataType: "json",
                success: function(result) {
                    swal(
                        'Success!',
                        'You clicked the button!',
                        'success'
                    )
                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');

                    window.parent.refreshTable();

                    // location.reload();

                },
                error: function(result) {
                    console.log('result',result);
                    if (result.responseJSON == undefined) {

                        $('#productForm').trigger("reset");
                        $('#ajaxModel').modal('hide');

                        window.parent.refreshTable();

                    } else {
                        var error = result.responseJSON.errors;

                        console.log('error', error);
                    }


                }
            });
        });


    });
</script>
