@extends('layouts.admin')

@section('breadcrumb1','log-manager')
@section('title_page','Logs')

@section('content')

@push('style')
@include('admin.components.datatable')

@include('admin.components.sweetalert')

@include('admin.components.select2')

<style>
    .swal-footer {
        text-align: center;
    }
</style>

@endpush


<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>Server Status</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-3">
                    <form id="statusForm">
                        <div class="form-group">
                            <label for="status">Pilih Status Server</label>
                            <select class="form-control" id="status" name="status">
                                <option value="1" {{ $data['status']==1 ? 'selected' : '' }}>Online</option>
                                <option value="0" {{ $data['status']==0 ? 'selected' : '' }}>Offline</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#statusForm').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url: "{{ route('serverStatus') }}",
                type: "POST",
                data: $('#status').serialize(),
                success: function (data) {
                    swal({
                        title: "Success!",
                        text: "Status Server Berhasil Diubah",
                        icon: "success",
                        button: "OK",
                    });

                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                },
                error: function (data) {
                    swal({
                        title: "Error!",
                        text: data.responseJSON.message,
                        icon: "error",
                        button: "OK",
                    });
                }
            });
        });
    });
</script>

@endpush

@endsection