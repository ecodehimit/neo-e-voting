<script type="text/javascript">
    $(document).ready(function() {

        function regenerate(id) {
            alert(id)
        }

        // $('.select2').select2({
        //     placeholder: "Select",
        //     allowClear: true
        // });

        $('.select2').select2({
            placeholder: 'Select an NRP',
            ajax: {
                url: "{{ route('feature.tokens.autocomplete') }}",
                dataType: 'json',
                delay: 250,
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.student_nrp + ' - ' + item.student_name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /* -------------- render datatables------------------------ */
        var table = $('#item').DataTable({
            language: {
                paginate: {
                    next: '<i class="fa fa-fw fa-long-arrow-right">',
                    previous: '<i class="fa fa-fw fa-long-arrow-left">'
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('feature.tokens.index') }}",
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'student.student_nrp',
                    name: 'student.student_nrp'
                },
                {
                    data: 'token',
                    name: 'token'
                },
                {
                    data: 'count',
                    // name: 'count',
                    orderable: false
                },
                {
                    data: 'duration',
                    name: 'duration'
                },
                {
                    data: 'updated_at',
                    name: 'updated_at'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
            ],

            "drawCallback": function(settings, json) {

                // alert( 'DataTables has finished its initialisation.' );
                $('.timer').each(function(i) {
                    var timer = $(this);
                    var id = $(this).attr('data-id');
                    var dateNow = $(this).attr('data-dateNow');
                    var time_in_minutes = 5;
                    var current_time = Date.parse($(this).attr('data-create').replace(' ',
                        'T'));
                    // console.log(current_time);
                    var deadline = new Date(current_time + time_in_minutes * 60 * 1000);
                    var status = $(this).attr('data-status');

                    // console.log(deadline);

                    function time_remaining(endtime) {

                        var tmp = '';
                        // php 2023-03-24T03:33:27.483859Z is date format date
                        var t = Date.parse(endtime) - Date.parse(new Date());

                        var seconds = Math.floor((t / 1000) % 60);
                        var minutes = Math.floor((t / 1000 / 60) % 60);
                        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
                        var days = Math.floor(t / (1000 * 60 * 60 * 24));
                        tmp = {
                            'total': t,
                            'days': days,
                            'hours': hours,
                            'minutes': minutes,
                            'seconds': seconds
                        };
                        // $.ajax({
                        //     // data: $('#itemForm').serialize(),
                        //     url: "{{ route('nowDate') }}",
                        //     type: "get",
                        //     dataType: 'json',
                        //     async: false,
                        //     global: false,
                        //     success: function (result) {


                        //     },

                        // });

                        // console.log(tmp);

                        return tmp

                    }

                    function run_clock(endtime) {

                        function update_clock() {
                            var t = time_remaining(endtime);
                            // console.log(t);
                            timer.text(t.minutes + 'm' + t.seconds + 's');
                            if (t.total <= 0) {
                                clearInterval(timeinterval);
                                timer.text("expired");
                                $.ajax({
                                    type: 'get',
                                    data: {
                                        id: id
                                    },
                                    url: "{{ route('feature.tokens.update') }}",
                                    success: function(data) {
                                        timer.prev().text('1');
                                    }
                                });
                            }

                            if (status == '1') {
                                timer.text("expired");
                            }

                        }
                        update_clock();
                        var timeinterval = setInterval(update_clock, 10000);
                    }
                    run_clock(deadline);
                });
            }
        });



        window.refreshTable = function() {
            table.draw();
        }

        $('#saveBtn').click(function(e) {
            e.preventDefault();
            let arr_select = ($('#select2').text()).split(' - ');
            arr_select[0] = arr_select[0].replace(/\s+/g, '');
            swal({
                title: "Are you sure?",
                text: `NRP : ${arr_select[0]}\n Name : ${arr_select[1]}`,
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        data: $('#itemForm').serialize(),
                        url: "{{ route('feature.tokens.store') }}",
                        type: "POST",
                        dataType: 'json',
                        success: function(result) {

                            swal(
                                'Success!',
                                'You clicked the button!',
                                'success'
                            )
                            $('#itemForm').trigger("reset");

                            window.parent.refreshTable();

                            // location.reload();

                        },
                        error: function(result) {
                            var response = JSON.parse(result.responseText);

                            swal(
                                'Error!',
                                response.message,
                                'error'
                            )
                            $('#itemForm').trigger("reset");
                            // if (result.responseJSON == undefined) {

                            //     $('#itemForm').trigger("reset");

                            // } else {
                            //     var error = result.responseJSON.errors;

                            //     console.log(error);
                            // }


                        }
                    });
                } else {
                    swal("Cancelled", "Cancel generates token", "error");
                }
                $('#select2').text('');
            });
        });

        $('body').on('click', '.regenerate', function() {

            var data_id = $(this).data('id');
            console.log($(this).data());
            swal({
                title: "Are you sure?",
                text: `NRP : ${$(this).data('nrp')}\n Name : ${$(this).data('name')}`,
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "get",
                        url: "{{ route('feature.tokens.regenerate') }}",
                        data: {
                            id: data_id
                        },
                        success: function(data) {
                            swal(
                                'Success!',
                                'Berhasil Regenerate Token',
                                'success'
                            )
                            table.draw();
                            console.log(data);
                        },
                        error: function(data) {
                            console.log(data.responseJSON);
                            swal(
                                'Error!',
                                data.responseJSON.message,
                                'error'
                            )
                        }
                    });
                } else {
                    swal("Cancelled", "you cancel the action :)", "error");
                }
            })


        });




    });
</script>
