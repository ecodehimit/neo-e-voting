@extends('layouts.admin')

@section('breadcrumb1','Feature')
@section('title_page','Token')

@section('content')

@push('style')
@include('admin.components.datatable')

@include('admin.components.sweetalert')

@include('admin.components.select2')

<style>
    .swal-footer {
        text-align: center;
    }
</style>

@endpush



<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>Create - Token</h6>

                {{-- <form action="{{route('feature.tokens.store')}}" method="post">--}}
                    <form id="itemForm" name="itemForm">
                        @csrf
                        <div class="row">
                            <div class="col-md-10 p-2">
                                <select class="select2 form-control" name="student_id" id="select2">


                                </select>
                            </div>
                            <div class="col-md-2 p-2">
                                <center>
                                    <button type="button" id="saveBtn" class="btn btn-warning" id="createNewProduct">
                                        Create</button>
                                </center>
                            </div>
                        </div>

                    </form>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>List - Token</h6>


            </div>
            @include('admin.components.message')
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-3">
                    <table class="table align-items-center mb-0" id="item">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">#</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">NRP
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Token</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Generate Status</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Duration Token</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Token time</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Action</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="ajaxModel" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="itemModal" name="itemModal"></div>
        </div>
    </div>

</div>

@push('script')


@endpush

@include('admin.contents.tokens.js')

@endsection
