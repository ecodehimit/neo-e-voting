<script type="text/javascript">
    $(document).ready(function() {
        // Requesting Datatable
        var table = $('#item').DataTable({
            language: {
                paginate: {
                    next: '<i class="fa fa-fw fa-long-arrow-right">',
                    previous: '<i class="fa fa-fw fa-long-arrow-left">'  
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('logs.index') }}",
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'description',
                    name: 'description',
                    searchable: true,
                },
                {
                    data: 'subject_type',
                    name: 'subject_type',
                    searchable: true,
                },
                {
                    data: 'causer_id',
                    name: 'causer_id',
                    searchable: true,
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    sortable: false,
                },
            ]

        });

        window.refreshTable = function() {
            table.draw();
        }
        $('#refresh').click(function() {
            window.parent.refreshTable();
        });
    })
</script>
