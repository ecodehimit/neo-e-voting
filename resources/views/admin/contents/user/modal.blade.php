<div class="alert alert-danger" style="display:none"></div>

<div class="modal-header" style="background-color: #2b3b98">
    <h4 class="modal-title text-white" id="modelHeading">Form</h4>
    <div class="block-options text-white">
        <a class="btn-block-option text-white" data-bs-dismiss="modal" aria-label="Close"
            style="background-color: transparent !important;">
            <i class="fa fa-fw fa-times"></i>
        </a>
    </div>
</div>


<form id="productForm" name="productForm" class="form-horizontal">
    <div class="modal-body">
        <input type="text" value="{{($data) ? $data->id :''}}" name="data_id" id="data_id"
            style="  visibility: hidden;">
        <div class="row">
            <div class="row push p-2">
                <div class="col-lg-12 col-xl-6">
                    <x-input type="text" name="nrp" label="NRP" value="{{($data) ? $data->nrp :''}}" placeholder="nrp"
                        required="" validateName="validate_nrp"></x-input>
                </div>
                <div class="col-lg-12 col-xl-6">
                    <x-input type="text" name="name" label="Name" value="{{($data) ? $data->name :''}}"
                        placeholder="name" required="" validateName="validate_name"></x-input>
                </div>
                <div class="col-lg-12 col-xl-6">
                    <x-input type="email" name="email" label="Email" value="{{($data) ? $data->email :''}}"
                        placeholder="email" required="" validateName="validate_email"></x-input>
                </div>
                <div class="col-lg-12 col-xl-6">
                    <!-- password -->
                    <x-input type="password" name="password" label="Password" placeholder="password" required=""
                        validateName="validate_password" value=""></x-input>
                </div>
                <div class="col-lg-12 col-xl-6">
                    <label for="role">Role</label>
                    <select name="role" id="role" class="form-control">
                        <option value="">-- Pilih Role --</option>
                        <option value="admin" {{($data) ? ($data->role == 'admin') ? 'selected' : '' : ''}}>Admin
                        </option>
                        <option value="panwaslu" {{($data) ? ($data->role == 'panwaslu') ? 'selected' : '' :
                            ''}}>Panwaslu
                        </option>
                        <option value="token" {{($data) ? ($data->role == 'token') ? 'selected' : '' : ''}}>Token
                        </option>
                        <option value="saksi" {{($data) ? ($data->role == 'saksi') ? 'selected' : '' : ''}}>Saksi
                        </option>
                    </select>
                    <div style="color: red" id="validate_role" name="validate_role"></div>


                </div>

                <div class="col-lg-12 col-xl-6">
                    <!-- description -->
                    <x-input type="text" name="description" label="Description" placeholder="description" required=""
                        validateName="validate_description" value="{{($data) ? $data->description :''}}"></x-input>
                </div>

            </div>
            <div class="block-content block-content-full text-end bg-body border-footer-modal">
                <button type="button" id="saveBtn" class="btn btn-sm btn-success">Save</button>
                <a class="btn btn-sm btn-alt-secondary" data-bs-dismiss="modal">Back</a>
            </div>
        </div>
</form>


<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2({
            placeholder: "Select",
            allowClear: true
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        $('#saveBtn').val("create-product");
        $('#productForm').trigger("reset");
        $('#saveBtn').click(function (e) {
            e.preventDefault();



            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: $('#productForm').serialize(),
                url: "{{ route('user-manager.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    swal(
                        'Success!',
                        'Data Berhasil Disimpan!',
                        'success'
                    )
                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    window.parent.refreshTable();
                }, error: function (data) {
                    if (data.responseJSON == undefined) {

                        $('#productForm').trigger("reset");
                        $('#ajaxModel').modal('hide');

                        window.parent.refreshTable();

                    } else {
                        var errors = data.responseJSON.errors;
                        console.log(errors);
                        if (errors.nrp) {
                            $('#validate_nrp').html(errors.nrp[0]);
                        } else {
                            $('#validate_nrp').html('');
                        }
                        if (errors.name) {
                            $('#validate_name').html(errors.name[0]);
                        } else {
                            $('#validate_name').html('');
                        }
                        if (errors.email) {
                            $('#validate_email').html(errors.email[0]);
                        } else {
                            $('#validate_email').html('');
                        }
                        if (errors.password) {
                            $('#validate_password').html(errors.password[0]);
                        } else {
                            $('#validate_password').html('');
                        }
                        if (errors.description) {
                            $('#validate_description').html(errors.description[0]);
                        } else {
                            $('#validate_description').html('');
                        }
                        if (errors.role) {
                            $('#validate_role').html(errors.role[0]);
                        } else {
                            $('#validate_role').html('');
                        }
                    }
                }
            });
        });


    });
</script>
