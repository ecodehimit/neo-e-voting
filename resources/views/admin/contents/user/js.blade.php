<script type="text/javascript">
    $(document).ready(function () {
        // Requesting Datatable
        var table = $('#item').DataTable({
            language: {
                paginate: {
                    next: '<i class="fa fa-fw fa-long-arrow-right">',
                    previous: '<i class="fa fa-fw fa-long-arrow-left">'  
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{route('user-manager.index')}}",
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'nrp',
                    name: 'nrp'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'role',
                    name: 'role'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
            ]

        });

        window.refreshTable = function () {
            table.draw();
        }
        $('#refresh').click(function () {
            window.parent.refreshTable();
        });

        // Requesting Modal
        $('#createNewProduct').click(function () {
            $('#itemModal').load('{{route("user-manager.create")}}');
            $('#ajaxModel').modal('show');
        });

        $('body').on('click', '.editProduct', function () {
            var data_id = $(this).data('id');
            $('#itemModal').load('{{route("user-manager.edit", ":id")}}'.replace(':id', data_id));
            $('#ajaxModel').modal('show');
        });

        // Requesting Delete
        $('body').on('click', '.deleteProduct', function () {

            var data_id = $(this).data('id');
            swal({
                title: "Are you sure?",
                // text: "",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: 'Success!',
                        text: 'you have deleted data!',
                        icon: 'success'
                    }).then(function () {

                        $.ajax({
                            type: "get",
                            url: "{{route('user-manager.delete', ':id')}}".replace(':id', data_id),
                            success: function (data) {
                                table.draw();
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });

                    });
                } else {
                    swal("Cancelled", "you cancel the action :)", "error");
                }
            })

        });
    })
</script>