@extends('layouts.admin')

@section('breadcrumb1','Group Manager')
@section('title_page','Groups')

@section('content')

@push('style')
@include('admin.components.datatable')

@include('admin.components.sweetalert')

@include('admin.components.select2')

<style>
    .swal-footer {
        text-align: center;
    }
</style>

@endpush


<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0">
                <h6>List - Group</h6>
                <a class="btn btn-warning" href="javascript:void(0)" id="createNewProduct"> Create</a>

            </div>
            @include('admin.components.message')
            <div class="card-body px-0 pt-0 pb-2">
                <div class="table-responsive p-3">
                    <table class="table align-items-center mb-0" id="item">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">#</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Group Name
                                </th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                    Action
                                </th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="ajaxModel" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="itemModal" name="itemModal"></div>
        </div>
    </div>

</div>

@push('script')


@endpush

@include('admin.contents.groups.js')

@endsection
