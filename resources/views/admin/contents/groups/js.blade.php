<script type="text/javascript">
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /* -------------- render datatables------------------------ */
        var table = $('#item').DataTable({
            language: {
                paginate: {
                    next: '<i class="fa fa-fw fa-long-arrow-right">',
                    previous: '<i class="fa fa-fw fa-long-arrow-left">'  
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{route('master.groups.index')}}",
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                {
                    data: 'group_name',
                    name: 'group_name'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
            ]
        });

        window.refreshTable = function () {
            table.draw();
        }

        /* --------------  modal create------------------------ */
        $('#refresh').click(function () {
            window.parent.refreshTable();
        });

        $('#createNewProduct').click(function () {
            $('#itemModal').load('{{route('master.groups.create')}}');

            $('#ajaxModel').modal('show');
        });

        /* --------------  modal edit------------------------ */
        $('body').on('click', '.editProduct', function () {
            var data_id = $(this).data('id');
            $('#itemModal').load('{{route('master.groups.index')}}' + '/' + data_id + '/edit');

            $('#ajaxModel').modal('show');
        });


        /*------------------------------------------
       --------------------------------------------
       Delete Product Code
       --------------------------------------------
       --------------------------------------------*/
        $('body').on('click', '.deleteProduct', function () {

            var data_id = $(this).data('id');
            swal({
                title: "Are you sure?",
                // text: "",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function (isConfirm) {
                if (isConfirm) {
                    swal({
                        title: 'Success!',
                        text: 'you have deleted data!',
                        icon: 'success'
                    }).then(function () {

                        $.ajax({
                            type: "get",
                            url: "{{route('master.groups.index')}}" + '/delete/' + data_id,
                            success: function (data) {
                                table.draw();
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });

                    });
                } else {
                    swal("Cancelled", "you cancel the action :)", "error");
                }
            })


        });

    });
</script>
