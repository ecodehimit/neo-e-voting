<div class="alert alert-danger" style="display:none"></div>

<div class="modal-header" style="background-color: #2b3b98">
    <h4 class="modal-title text-white" id="modelHeading">Form</h4>
    <div class="block-options text-white">
        <a class="btn-block-option text-white" data-bs-dismiss="modal" aria-label="Close"
            style="background-color: transparent !important;">
            <i class="fa fa-fw fa-times"></i>
        </a>
    </div>
</div>


<form id="productForm" name="productForm" class="form-horizontal">
    <div class="modal-body">
        <input type="text" value="{{($data) ? $data->id :''}}" name="data_id" id="data_id"
            style="  visibility: hidden;">
        <div class="row">
            <div class="row push p-2">
                <div class="col-lg-12 col-xl-6">
                    <x-input type="text" name="group_name" label="Group Name"
                        value="{{($data) ? $data->group_name :''}}" placeholder="group_name" required=""
                        validateName="validate_group_name"></x-input>

                </div>
            </div>
        </div>
        <div class="block-content block-content-full text-end bg-body border-footer-modal">
            {{-- <a id="saveBtn" class="btn btn-sm btn-primary">Save</a>--}}
            <button type="button" id="saveBtn" class="btn btn-sm btn-success">Save</button>
            <a class="btn btn-sm btn-alt-secondary" data-bs-dismiss="modal">Back</a>
        </div>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2({
            placeholder: "Select",
            allowClear: true
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#saveBtn').val("create-product");
        $('#productForm').trigger("reset");



        /*---------------------------------------------
           --------------------------------------------
           Action Items
           --------------------------------------------
           --------------------------------------------*/
        $('#saveBtn').click(function (e) {
            e.preventDefault();
            console.log('testing data', $('#productForm').serialize());

            $.ajax({
                data: $('#productForm').serialize(),
                url: "{{ route('master.groups.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    swal(
                        'Success!',
                        'You clicked the button!',
                        'success'
                    )
                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');

                    window.parent.refreshTable();

                    // location.reload();

                },
                error: function (result) {
                    // console.log(result);
                    if (result.responseJSON == undefined) {

                        $('#productForm').trigger("reset");
                        $('#ajaxModel').modal('hide');

                        window.parent.refreshTable();

                    } else {
                        var error = result.responseJSON.errors;

                        console.log(error);
                        //
                        if (error.group_name != undefined) {
                            $('#validate_group_name').html('*' + error.group_name[0]);
                        } else {
                            $('#validate_group_name').html('');
                        }
                    }


                }
            });
        });


    });
</script>
