@extends('layouts.admin')
@section('breadcrumb1', 'Page')
@section('title_page', 'Dashboard')

@section('content')

    @push('style')
        <style>
            #countdown {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                height: 200px;
                width: 200px;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            #countdown-number {
                font-size: 40px;
                color: #3A416F;
                display: inline-block;
                line-height: 40px;
            }

            #svg {
                position: absolute;
                top: 0;
                right: 0;
                width: 200px;
                height: 200px;
                transform: rotateY(-180deg) rotateZ(-90deg);
            }

            #svg circle {
                display: none;
                stroke-dasharray: 681px;
                stroke-dashoffset: 0px;
                stroke-linecap: round;
                stroke-width: 10px;
                stroke: #fbcf33;
                fill: none;
                filter: drop-shadow(2px 2px 4px grey)
                    /* animation: countdown 5s linear forwards; */
            }

            @keyframes countdown {
                from {
                    stroke-dashoffset: 0px;
                }

                to {
                    stroke-dashoffset: 681px;
                }
            }
        </style>
    @endpush

    @push('script')
        <script>
           function showVote()
           {
                var data = {!! $data !!};
                var label = [];
                var suara = [];
                
                for(const property in data)
                {
                    label.push(data[property].label);
                    suara.push(data[property].suara);
                }

                const ctx = document.getElementById('chart-bars').getContext('2d');

                const configChart = {
                    type : 'bar',
                    data : {
                        labels : label,
                        datasets : [{
                            label : 'Suara',
                            data : suara,
                            backgroundColor : [
                                '#3A416F',
                            ],
                            borderColor : [
                                '#3A416F',
                            ],
                            borderWidth : 1
                        }]
                    },
                    options : {
                        scales : {
                            y : {
                                beginAtZero : true
                            }
                        },
                        plugins : {
                            legend : {
                                display : false
                            }
                        }
                    }

                }
                new Chart(ctx, configChart);
           }

           function countDown()
           {
                // display countdown from svg circle 5 seconds
                var countdown = 6;
                var countdownNumber = document.getElementById('countdown-number');
                var countdownCircle = document.querySelector('#svg circle');
                var countdownInterval = setInterval(function() {
                    countdown--;
                    countdownNumber.innerHTML = countdown;
                    countdownCircle.style.animation = 'countdown ' + countdown + 's linear forwards';

                    if (countdown === 0) {
                        clearInterval(countdownInterval);
                        countdownNumber.style.display = 'none';
                        countdownCircle.style.display = 'none';
                        showVote();
                    }
                }, 1000);



           }

           $(document).ready(function() {

                $('#showVote').click(function() {
                    countDown();
                });

           });
        </script>

        <script>
            var win = navigator.platform.indexOf('Win') > -1;
            if (win && document.querySelector('#sidenav-scrollbar')) {
                var options = {
                    damping: '0.5'
                }
                Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
            }
        </script>
    @endpush
    <div class="col-lg-12">
        <div class="card z-index-2">
            <div class="card-header pb-0 d-flex justify-content-between">
                <h6>Announcement overview</h6>
                <div id="showVote" class="btn btn-dark">show vote</div>
            </div>
            <div class="card-body p-3 position-relative">
                <div id="countdown">
                    <div id="countdown-number" class="h5 text-center">
                        <img src="{{ asset('assets_softUI/img/evote_logo.png') }}" alt="logo" width="100px">
                        <br>
                        <div style="margin-bottom: -15px !important;">
                            <small style="font-size: 13px;">Developed by</small>
                        </div>
                        <img src="{{ asset('assets_softUI/img/ecode.png') }}" alt="logo" width="100px">
                    </div>
                    <svg id="svg">
                        <circle r="90" cx="100" cy="100"></circle>
                    </svg>
                </div>
                <div class="chart">
                    <canvas id="chart-bars" class="chart-canvas" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection
