<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Pemilih</title>
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <link rel="stylesheet" href="{{ asset('/assets/style.css') }}">
    {!! ReCaptcha::htmlScriptTagJsApi() !!}

</head>

<body>


    <section id="login-section" class="container-fluid">
        <div class="row align-items-center h-100" style="height: 100vh !important">
            <div class="col-md-6 d-none d-md-block">
                <div class="row h-100 align-items-center" id="log-left">
                    <div class="col-12">
                        <div class="card-glass p-5">
                            <h1 class="text-white">Login Untuk Vote <br> <span
                                    class="text-warning">#TheNextLeader</span> Pilihanmu</h1>
                            <div class="row mt-4">
                                <div class="col">
                                    <h5 class="text-white"><small>Cuma Butuh NRP dan Token aja kok</small></h5>
                                </div>
                                <div class="col">
                                    <img src="{{ asset('/assets/img/arrow.png') }}" class="img-fluid"
                                        style="max-height: 15px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-5">
                <h1><strong>Login</strong></h1>
                <form action="{{ route('actionLogin.student') }}" class="mt-5" method="post">
                    @method('POST')
                    @include('admin.components.message')
                    @csrf
                    <div class="form-group">
                        <label for="nrp">
                            <h5>NRP</h5>
                        </label>
                        <input type="text" class="form-control p-3 rounded-3 mt-2" id="nrp" placeholder="Masukkan NRP"
                            style="max-width: 70%;" name="nrp">
                    </div>
                    <div class="form-group mt-3">
                        <label for="token">
                            <h5>Token</h5>
                        </label>
                        <input type="text" class="form-control p-3 rounded-3 mt-2" id="token"
                            placeholder="Masukkan Token" style="max-width: 70%;" name="token">
                    </div>
                    <div class="form-group mt-3">
                        {!! htmlFormSnippet() !!}
                    </div>
                    <button class="mt-3 px-5 py-2 btn btn-primary text-center" type="submit"
                        style="width: 70%;">Login</button>
                </form>
            </div>
        </div>
    </section>
    <script src="./assets/theme.js"></script>
</body>

</html>