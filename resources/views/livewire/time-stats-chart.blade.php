@push('script')
    <script>
        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        var ctx2 = document.getElementById("chart-line").getContext("2d");

        var gradientStroke1 = ctx2.createLinearGradient(0, 230, 0, 50);

        gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.2)');
        gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

        var gradientStroke2 = ctx2.createLinearGradient(0, 230, 0, 50);

        gradientStroke2.addColorStop(1, 'rgba(20,23,39,0.2)');
        gradientStroke2.addColorStop(0.2, 'rgba(72,72,176,0.0)');
        gradientStroke2.addColorStop(0, 'rgba(20,23,39,0)'); //purple colors

        var calculations = {!! json_encode($calculation->toArray()) !!};

        const groups = calculations.reduce((groups, game) => {
            var date = new Date(Date.parse(game['created_at']));
            var h = date.getHours();
            var m = date.getMinutes();
            h = checkTime(h);
            m = checkTime(m);
            date.setSeconds(0);
            var dateFormat = `${h}:${m}`;
            date = dateFormat;
            if (!groups[date]) {
                groups[date] = [];
            }
            groups[date].push(game);
            return groups;
        }, {});

        const groupArrays = Object.keys(groups).map((date) => {
            return {
                x: date,
                y: groups[date].length
            };
        });

        console.log('group arrays',groupArrays)

        var now = new Date();
        now.setMinutes(now.getMinutes() + 30);
        const data = groupArrays;

        new Chart(ctx2, {
            type: "line",
            data: {
                datasets: [{
                    label: "Voter",
                    tension: 0.4,
                    borderWidth: 0,
                    pointRadius: 0,
                    borderColor: "#cb0c9f",
                    borderWidth: 3,
                    backgroundColor: gradientStroke1,
                    fill: true,
                    data: data,
                    maxBarThickness: 6

                }],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            padding: 10,
                            color: '#b2b9bf',
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                    x: {
                        bounds: 'ticks',
                        type: 'time',
                        time: {
                            tooltipFormat: 'hh:mm a'
                        },
                        grid: {
                            drawBorder: false,
                            display: false,
                            drawOnChartArea: false,
                            drawTicks: false,
                            borderDash: [5, 5]
                        },
                        ticks: {
                            display: true,
                            color: '#b2b9bf',
                            padding: 20,
                            font: {
                                size: 11,
                                family: "Open Sans",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
        console.log('ctx2',ctx2);
    </script>
@endpush

<div class="col-lg-7">
    <div class="card z-index-2">
        <div class="card-header pb-0">
            <h6>Statistik Waktu</h6>
        </div>
        <div class="card-body p-3">
            <div class="chart">
                <canvas id="chart-line" class="chart-canvas" height="300"></canvas>
            </div>
        </div>
    </div>
</div>
