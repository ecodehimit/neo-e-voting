<style>
    .swal-padding {
        padding-top: 2rem;
        padding-bottom: 3rem;
    }
</style>

<div wire:poll.1s class="col-lg-5 mb-lg-0 mb-4">
    <div class="card z-index-2">
        <div class="card-body p-3" id="card-saksi">
            @php
                $saksiLengkap = 0;
            @endphp
            <b>Jumlah Saksi: </b>{{ $jumlahSaksi }}
            <table style="width: 100%">
                <tr>
                    <th>No</th>
                    <th>Saksi</th>
                    <th>Keterangan</th>
                    <th>Sudah Login</th>
                </tr>
                @foreach ($saksi as $user)
                    <tr>
                        @php
                            $status = App\Http\Controllers\HomeController::getLoginStatusByUserId($user->id);
                            if ($status == 'Sudah') {
                                $saksiLengkap++;
                            }
                        @endphp
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->description }}</td>
                        <td>{{ $status }}</td>
                    </tr>
                @endforeach
            </table>

            <?php if(Auth::user()->role == "admin"): ?>
                <button class="btn mt-3" @disabled($saksiLengkap != $jumlahSaksi || !$kalkulasi)><a href="{{ route('dashboard.announcement', $saksiLengkap) }}">Cek Pengumuman</a></button>
            <?php endif; ?>
        </div>
    </div>
</div>
