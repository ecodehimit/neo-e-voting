<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('assets_softUI/img/evote_logo.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>NEO E-Voting </title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');

        /* This pen */
        body {
            font-family: 'Poppins', sans-serif;
            text-rendering: optimizeLegibility;
            font-weight: initial;
        }

        .postcard {
            overflow: hidden;
        }

        .postcard_img {
            max-height: 100%;
            height: 350px;
            width: 100%;
            object-fit: cover;
            position: relative;
            object-position: center;
            transition: transform 0.3s ease;
        }

        .card:hover .postcard_img {
            transform: scale(1.1);
        }

        .dropdown-item:hover {
            background-color: rgba(var(--bs-danger-rgb), var(--bs-bg-opacity)) !important;
        }

        .card_candidate {
            background-color: white;
            box-shadow: 7px 7px 0px 0px rgba(0, 0, 0, 1);
            -webkit-box-shadow: 10px 10px 0px 0px rgba(0, 0, 0, 1);
            -moz-box-shadow: 10px 10px 0px 0px rgba(0, 0, 0, 1);
            border: 2px solid black;
            height: 100%;
        }

        .card_visi_misi {
            min-height: 400px;
            border: none;
            box-shadow: 0px 0px 20px 16px rgba(240, 240, 240, 0.75);
        }

        .swal-footer {
            text-align: center;
        }

        .img_success {
            animation: scaly 1.5s alternate infinite ease-in;
        }

        .card_succes {
            animation-name: fade;
            animation-duration: 3s;
            animation-fill-mode: forwards;
        }

        @keyframes scaly {
            0% {
                transform: scale(.9);
            }

            100% {
                transform: scale(1.1);
            }
        }

        @keyframes fade {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }
    </style>



</head>

<body class="d-flex justify-content-center align-items-center vh-100">

    {{--    @dd(auth()->guard('voter')->user()->token) --}}

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 col-sm-8">
                <div class="card_succes px-4 py-5 text-center">
                    <div class="bg-curve"></div>
                    <img class="d-block mx-auto mb-4 img_success"
                        src="{{ asset('./assets_softUI/img/evote_logo.png') }}" alt="" width="200">
                    <h1 class="display-5 fw-bold">Voting Success</h1>
                    <div class="col-lg-12">
                        <p class="lead mb-4">{{ session()->get('thank') }}
                        </p>
                        <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <a href="{{ route('index') }}" class="btn btn-primary btn-lg">Back to
                                Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./assets/theme.js"></script>
</body>

</html>
