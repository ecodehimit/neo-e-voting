<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{ asset('assets_softUI/img/evote_logo.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>NEO E-Voting | Offline Service</title>
    <meta name="description" content="Yahh servernya mati >.<">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');

        /* This pen */
        body {
            font-family: 'Poppins', sans-serif;
            text-rendering: optimizeLegibility;
            font-weight: initial;
            /*background: linear-gradient(#141e30, #184677);*/
            background: rgb(244, 244, 244);
        }

        .postcard {
            overflow: hidden;
        }

        .postcard_img {
            max-height: 100%;
            height: 350px;
            width: 100%;
            object-fit: cover;
            position: relative;
            object-position: center;
            transition: transform 0.3s ease;
        }

        .card:hover .postcard_img {
            transform: scale(1.1);
        }

        .dropdown-item:hover {
            background-color: rgba(var(--bs-danger-rgb), var(--bs-bg-opacity)) !important;
        }

        .card_candidate {
            background-color: white;
            box-shadow: 7px 7px 0px 0px rgba(0, 0, 0, 1);
            -webkit-box-shadow: 10px 10px 0px 0px rgba(0, 0, 0, 1);
            -moz-box-shadow: 10px 10px 0px 0px rgba(0, 0, 0, 1);
            border: 2px solid black;
            height: 100%;
        }

        .card_visi_misi {
            min-height: 400px;
            border: none;
            box-shadow: 0px 0px 20px 16px rgba(240, 240, 240, 0.75);
        }

        .swal-footer {
            text-align: center;
        }

        .img_success {
            animation: scaly 1.5s alternate infinite ease-in;
        }

        .card_succes {
            animation-name: fade;
            animation-duration: 3s;
            animation-fill-mode: forwards;
        }

        @keyframes scaly {
            0% {
                transform: scale(.9);
            }

            100% {
                transform: scale(1.1);
            }
        }

        @keyframes fade {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }
    </style>



</head>

<body class="d-flex justify-content-center align-items-center vh-100">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10 col-sm-8">
                <div class="card_succes px-4 py-5 text-center">
                    <div class="bg-curve"></div>
                    <img class="d-block mx-auto mb-4 img_success"
                        src="{{ asset('./assets_softUI/img/evote_logo.png') }}" alt="" width="200">
                    <h1 class="display-5 fw-bold">🗿</h1>
                    <div class="col-lg-12">
                        <p class="lead mb-4">Yah, Servernya di matiin, coba hubungi admin</p>
                        </p>
                        <footer class="text-center container">
                            <hr>
                            <p>
                                ©2023 eVote by E-CODE HIMIT PENS. Made with 💙 from Surabaya
                            </p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
        </script>


</body>

</html>