<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>


    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{asset('assets_softUI/img/evote_logo.png')}}">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    <meta name="csrf-token" content="{{ csrf_token() }}">
    @livewireStyles
    <title>NEO E-Voting</title>

{{--    <title>{{ config('app.name', 'Laravel') }}</title>--}}

    @include('admin.components.top')
    @stack('style')

<!-- Scripts -->
{{--    @vite(['resources/sass/app.scss', 'resources/js/app.js'])--}}
</head>
<body class="g-sidenav-show  bg-gray-100">
    <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
        <div class="sidenav-header">
            <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
            <a class="navbar-brand m-0" href="https://suksesi.himitpens.com/" target="_blank">
                <img src="{{asset('assets_softUI/img/evote_logo.png')}}" class="navbar-brand-img h-100" alt="main_logo">
                <span class="ms-1 font-weight-bold">NEO E-Voting</span>
            </a>
        </div>
        <hr class="horizontal dark mt-0">


        @include('admin.partials.sidebar')
    </aside>

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        @include('admin.partials.nav')

        <div class="container-fluid py-4">

            @yield('content')
            @include('admin.partials.footer')

        </div>

    </main>


    @include('admin.components.bottom')

    @stack('script')
    @livewireScripts
</body>
</html>
