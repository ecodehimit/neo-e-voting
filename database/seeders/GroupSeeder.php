<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //D4 IT
        for ($i = 2020; $i <= 2023; $i++) {
            $d4ITA = new Group();
            $d4ITA->group_name = 'D4 IT A ' . $i;
            $d4ITA->save();

            $d4ITB = new Group();
            $d4ITB->group_name = 'D4 IT B ' . $i;
            $d4ITB->save();
        }


        //D3 IT
        for ($i = 2021; $i <= 2023; $i++) {
            $d3ITA = new Group();
            $d3ITA->group_name = 'D3 IT A ' . $i;
            $d3ITA->save();

            $d3ITB = new Group();
            $d3ITB->group_name = 'D3 IT B ' . $i;
            $d3ITB->save();
        }

        //D4 DS
        for ($i = 2021; $i <= 2023; $i++) {
            if ($i == 2023) {
                $d4DSA = new Group();
                $d4DSA->group_name = 'D4 DS A ' . $i;
                $d4DSA->save();

                $d4DSB = new Group();
                $d4DSB->group_name = 'D4 DS B ' . $i;
                $d4DSB->save();
            } else {
                $d4DS = new Group();
                $d4DS->group_name = 'D4 DS ' . $i;
                $d4DS->save();
            }
        }
    }
}
