<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array(
            array(
                'nrp' => '3122640015',
                'name' => 'Rizky Putra Ramadan',
                'email' => 'rizky@evotelab.com',
                'password' => Hash::make('rizky'),
                'role' => 'admin',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ),array(
                'nrp' => '3122640016',
                'name' => 'Zake Ipsum',
                'email' => 'panwaslu@evotelab.com',
                'password' => Hash::make('panwaslu'),
                'role' => 'panwaslu',
                'description' => 'Panwaslu',
                'created_at' => now(),
                'updated_at' => now(),
            ),array(
                'nrp' => '3122640017',
                'name' => 'Token',
                'email' => 'gentoken@evotelab.com',
                'password' => Hash::make('token'),
                'role' => 'token',
                'description' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ),array(
                'nrp' => '3122640018',
                'name' => 'Jane Doe',
                'email' => 'saksi1@evotelab.com',
                'password' => Hash::make('saksi1'),
                'role' => 'saksi',
                'description' => 'Saksi Calon 1',
                'created_at' => now(),
                'updated_at' => now(),
            ),array(
                'nrp' => '3122640019',
                'name' => 'John Doe',
                'email' => 'saksi2@evotelab.com',
                'password' => Hash::make('saksi2'),
                'role' => 'saksi',
                'description' => 'Saksi Calon 2',
                'created_at' => now(),
                'updated_at' => now(),
            ),
        );
        User::insert($data);
    }
}
