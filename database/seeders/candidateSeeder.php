<?php

namespace Database\Seeders;

use App\Models\Candidate;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class candidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'candidate_lead' => 20,
                'candidate_vice' => 21,
                'number' => '1',
                'photo' => 'assets_softUI/img/avatar.png',
                'visi' => 'ini visi 1',
                'mission' => 'ini misi 1',
                'created_at' => now(),
                'updated_at' => now(),
            ),array(
                'candidate_lead' => 22,
                'candidate_vice' => 23,
                'number' => '2',
                'photo' => 'assets_softUI/img/avatar.png',
                'visi' => 'ini visi 2',
                'mission' => 'ini visi 2',
                'created_at' => now(),
                'updated_at' => now(),
            ),array(
                'candidate_lead' => null,
                'candidate_vice' => null,
                'number' => '0',
                'photo' => 'suara kosong',
                'visi' => 'suara kosong',
                'mission' => 'suara kosong',
                'created_at' => now(),
                'updated_at' => now(),
            ),
        );
        Candidate::insert($data);
    }
}
