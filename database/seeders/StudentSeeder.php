<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 50; $i++) {

            $data = new Student();
            $data->group_id = $faker->numberBetween(1, 14);
            $data->student_name = $faker->name;
            $data->student_nrp = '31226400' . $i;
            $data->status = 'no';
            $data->save();
        }
    }
}
