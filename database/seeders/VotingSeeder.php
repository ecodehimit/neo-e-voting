<?php

namespace Database\Seeders;

use App\Models\Calculation;
use App\Models\Student;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class VotingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = Student::all();
        $faker = Faker::create('id_ID');

        foreach ($students as $student) {

            $encrypted = Crypt::encryptString($faker->numberBetween(1, 2));

            $data = new Calculation();
            $data->student_id = $student->id;
            $data->candidate_choice = $encrypted;

            $data->save();
        }
    }
}
